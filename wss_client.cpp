#include "wss_client.h"
#include <sys/time.h>
#include <sstream>
#include <unistd.h>
#include <thread>

WssClient::WssClient()
    : SocketDataCallback    ()
    , WsProtocol            (this)
    // , ssl_client_           (new SslClient)
    // , socket_client_        (new SocketClient)
{
}
WssClient::~WssClient()
{
}

//bool WssClient::WssInit() {
//    bool connected = false;
//    SetState(SocketState::ConnectingState);

//    auto guard = Wss::qScopeGuard([this, &connected]{
//        SetState(connected ? SocketState::ConnectedState
//                           : SocketState::UnconnectedState);
//    });

//    if (!socket_client_->Connect(/*"127.0.0.1", 8888, AF_INET*/)) {
//        IF_LOG("Connect failed");
//        return false;
//    }
//    IF_LOG("client fd: " << socket_client_->Fd());
//    if (!ssl_client_->SslInit(socket_client_->Fd())) {
//        IF_LOG("SslInit failed");
//        return false;
//    }
//    Wss::SetSocktTimeout(socket_client_->Fd(), 2);
//    if (!Handshark()) {
//        IF_LOG("Handshark failed");
//        return false;
//    }
//    connected = true;
//    return true;
//}
int64_t WssClient::Read(void *data, int64_t size) {
    return ssl_client_->Read(data, size);
}
int64_t WssClient::Write(void *data, int64_t size) {
    return ssl_client_->Write(data, size);
}
bool WssClient::maskEnabled () {
    return true;
}

void WssClient::OnTextFrame(const std::string &text, bool isFinalFrame) {
    // IF_LOG("OnTextFrame: " << text << " is final: " << isFinalFrame);
}
void WssClient::OnTextMessage(const std::string &text) {
    IF_LOG("OnTextMessage: " << text);
}
void WssClient::OnBinaryFrame(const DataArray &data, bool isFinalFrame) {
    IF_LOG("OnBinaryFrame: " << data.size());
}
void WssClient::OnBinaryMessage(const DataArray &data) {
    //    for (int i = 0; i < data.size(); ++i) {
    //        if (i < 5 || i > (data.size() - 5))
    //            std::cout << data[i] << " ";

    //        if (i >= 5 && i <= (data.size() - 5)) {
    //            continue;
    //        }
    //    }
    //    std::cout << " data size1: " << data.size() << std::endl;
}
void WssClient::OnFailedMessage(const std::string &reason) {
    IF_LOG("OnFailedMessage: " << reason);
}

bool WssClient::Handshark() {
    std::string message;
    struct timeval time;
    gettimeofday(&time, nullptr);
    long now = time.tv_sec*1000 + time.tv_usec/1000;

    int Sec_WebSocket_Version = 13;
    std::stringstream stream;
    stream << Sec_WebSocket_Version << "+" << now;
    std::string key_str;
    stream >> key_str;

    char Sec_WebSocket_Key[25] = {0};
    int bytes = Wss::Base64Encode(key_str.c_str(), key_str.length(), Sec_WebSocket_Key);
    IF_LOG(key_str << " " << bytes << " " << key_str.length());
    IF_LOG(Sec_WebSocket_Key << " " << strlen(Sec_WebSocket_Key));

    const char *request = {
        "GET / HTTP/1.1\r\n"
        "Connection: Upgrade\r\n"
        "Host: %s:%d\r\n"
        "Sec-WebSocket-Key: %s\r\n"
        "Sec-WebSocket-Version: %d\r\n"
        "Upgrade: websocket\r\n"
        "\r\n"
    };

    char requestHead[256] = {0};
    // 应该是指定一个服务端的server、port，服务端创建，然后客户端wss去连接
    snprintf(requestHead, sizeof(requestHead), request,
             socket_client_->Addr().ToString().c_str(),
             socket_client_->Port(),
             Sec_WebSocket_Key,
             Sec_WebSocket_Version);

    int ret = Write(requestHead, strlen(requestHead));
    IF_LOG("Write: " << ret);

    char buffer[256] = {0};
    ret = Read(buffer, sizeof(buffer));
    if (ret == -1 || ret == 0) {
        IF_LOG("Read error");
        return -1;
    }
    IF_LOG("Read bytes: " << ret);
    IF_LOG(buffer);
    std::string header(buffer);
    if (header.find_first_of("Connection: Upgrade") == std::string::npos) {
        IF_LOGFE("Failed! [Connection: Upgrade] info not found.");
        return false;
    }
    SetState(SocketState::ConnectedState);
    return true;
}

#if BUILD_WSS_MAIN
int main() {
    WssClient client;
    if (client.WssInit()) {
        IF_LOG("Upgrade success");
    }

    client.sendTextMessage("{ \"test_\" : 1 }");
    client.sendTextMessage("{ \"test_\" : 1 }");
    client.sendTextMessage("{ \"test_\" : 1 }");
    client.sendTextMessage("{ \"test_\" : 1 }");

    usleep(100);

    return 0;
}

#endif
