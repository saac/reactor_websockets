#ifndef SSL_CLIENT_H
#define SSL_CLIENT_H
#include "common.hpp"
#include <netinet/tcp.h>  // struct tcp_info
#include "ssl_base.h"

class SslClient : public SslBase {
public:
    bool SslInit(int fd);
    void SslInfo();
    SslClient();
    ~SslClient();
    bool Init(int fd) override {
        if (m_sslType != SslBase::Unknow) {
            return true;
        }
        m_sslType = SslBase::Client;
        return SslInit(fd);
    }
    int Write(void *data, size_t len);
    int Read(void *buf, size_t len);
    // void Close() override { }

    bool SslHandShake();
    SslState ConnectState() {
        return m_state;
    }
    bool WantRead();
    bool WantWrite();
private:
    SSL_CTX     *ctx_;           // SSL上下文句柄
    SSL         *ssl_;           // SSL结构体指针
    // X509     *server_cert_;   // X509 结构体，用户保存服务器端证书
    SSL_METHOD  *meth_;          // SSL 协议

    SslState    m_state;
    int want_read_write_;
    BIO         *errBio_;
};

#endif//SSL_CLIENT_H
