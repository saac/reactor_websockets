#include "physical_socket_server.h"
#include "ep_common.h"
#include "signaler.h"

PhysicalSocketServer::PhysicalSocketServer()
    : epoll_fd_(epoll_create(FD_SETSIZE))
    , fWait_(false)
    , signal_wakeup_(nullptr)
{
    XASSERT(epoll_fd_ != -1);
    // The `fWait_` flag to be cleared by the Signaler.
    // signal_wakeup_ = new Signaler(this, fWait_);
}
PhysicalSocketServer::~PhysicalSocketServer() {
    if (signal_wakeup_) {
        delete signal_wakeup_;
    }
    if (epoll_fd_ != INVALID_SOCKET) {
        closesocket(epoll_fd_);
    }
    XASSERT(dispatcher_by_key_.empty());
    XASSERT(key_by_dispatcher_.empty());
}

AsyncSocket* PhysicalSocketServer::CreateAsyncSocket(int family, int type) {
    SocketDispatcher* dispatcher = new SocketDispatcher(this);
    IF_LOGW("PhysicalSocketServer::CreateAsyncSocket: create dispatcher");
    if (dispatcher->Create(family, type)) {
        return dispatcher;
    } else {
        delete dispatcher;
        return nullptr;
    }
}

AsyncSocket* PhysicalSocketServer::WrapSocket(SOCKET s) {
    SocketDispatcher* dispatcher = new SocketDispatcher(s, this);
    if (dispatcher->Initialize()) {
        return dispatcher;
    } else {
        delete dispatcher;
        return nullptr;
    }
}

bool PhysicalSocketServer::Wait(int cmsWait, bool process_io) {
    if (process_io && epoll_fd_ != INVALID_SOCKET) {
        return WaitEpoll(cmsWait);
    }
}

// 没有用到
void PhysicalSocketServer::WakeUp() {
    signal_wakeup_->Signal();
}

void PhysicalSocketServer::Add(Dispatcher* pdispatcher) {
    Wss::CritScope cs(&crit_);
    if (key_by_dispatcher_.count(pdispatcher)) {
        IF_LOG("PhysicalSocketServer asked to add a duplicate dispatcher.");
        return;
    }
    uint64_t key = next_dispatcher_key_++;
    dispatcher_by_key_.emplace(key, pdispatcher);
    key_by_dispatcher_.emplace(pdispatcher, key);
#if defined(WEBRTC_USE_EPOLL)
    if (epoll_fd_ != INVALID_SOCKET) {
        AddEpoll(pdispatcher, key);
    }
#endif  // WEBRTC_USE_EPOLL
}

void PhysicalSocketServer::Remove(Dispatcher* pdispatcher) {
    Wss::CritScope cs(&crit_);
    if (!key_by_dispatcher_.count(pdispatcher)) {
        IF_LOG("PhysicalSocketServer asked to remove a unknown "
               "dispatcher, potentially from a duplicate call to Add.");
        return;
    }
    uint64_t key = key_by_dispatcher_.at(pdispatcher);
    IF_LOGW("PhysicalSocketServer::Remove start " << key);
    key_by_dispatcher_.erase(pdispatcher);
    dispatcher_by_key_.erase(key);
#if defined(WEBRTC_USE_EPOLL)
    if (epoll_fd_ != INVALID_SOCKET) {
        RemoveEpoll(pdispatcher);
    }
#endif  // WEBRTC_USE_EPOLL
    IF_LOGW("PhysicalSocketServer::Remove end " << key);
}

void PhysicalSocketServer::Update(Dispatcher* pdispatcher) {
#if defined(WEBRTC_USE_EPOLL)
    if (epoll_fd_ == INVALID_SOCKET) {
        return;
    }

    // Don't update dispatchers that haven't yet been added.
    Wss::CritScope cs(&crit_);
    if (!key_by_dispatcher_.count(pdispatcher)) {
        return;
    }

    UpdateEpoll(pdispatcher, key_by_dispatcher_.at(pdispatcher));
#endif
}

void PhysicalSocketServer::AddEpoll(Dispatcher* pdispatcher, uint64_t key) {
    XASSERT(epoll_fd_ != INVALID_SOCKET);
    int fd = pdispatcher->GetDescriptor();
    XASSERT(fd != INVALID_SOCKET);
    if (fd == INVALID_SOCKET) {
        return;
    }
    IF_LOGW("PhysicalSocketServer::AddEpoll: " << pdispatcher << " key: " << key);
    struct epoll_event event = {0};
    event.events = GetEpollEvents(pdispatcher->GetRequestedEvents());
    event.data.u64 = key;
    IF_LOGW("epoll_ctl ADD: " << fd);
    int err = epoll_ctl(epoll_fd_, EPOLL_CTL_ADD, fd, &event);
    XASSERT(err == 0);
    if (err == -1) {
        IF_LOG("epoll_ctl EPOLL_CTL_ADD: " << errno);
    }
}
void PhysicalSocketServer::RemoveEpoll(Dispatcher* pdispatcher) {
    XASSERT(epoll_fd_ != INVALID_SOCKET);
    int fd = pdispatcher->GetDescriptor();
    XASSERT(fd != INVALID_SOCKET);
    if (fd == INVALID_SOCKET) {
        return;
    }

    struct epoll_event event = {0};
    int err = epoll_ctl(epoll_fd_, EPOLL_CTL_DEL, fd, &event);
    XASSERT(err == 0 || errno == ENOENT);
    if (err == -1) {
        if (errno == ENOENT) {
            // Socket has already been closed.
            IF_LOG("epoll_ctl EPOLL_CTL_DEL: " << errno);
        } else {
            IF_LOG("epoll_ctl EPOLL_CTL_DEL");
        }
    }
}

void PhysicalSocketServer::UpdateEpoll(Dispatcher* pdispatcher, uint64_t key) {
    XASSERT(epoll_fd_ != INVALID_SOCKET);
    int fd = pdispatcher->GetDescriptor();
    XASSERT(fd != INVALID_SOCKET);
    if (fd == INVALID_SOCKET) {
        return;
    }
    IF_LOGW("PhysicalSocketServer::UpdateEpoll: " << pdispatcher << " key: " << key);

    struct epoll_event event = {0};
    event.events = GetEpollEvents(pdispatcher->GetRequestedEvents());
    event.data.u64 = key;
    int err = epoll_ctl(epoll_fd_, EPOLL_CTL_MOD, fd, &event);
    XASSERT(err == 0);
    if (err == -1) {
        IF_LOGW("epoll_ctl EPOLL_CTL_MOD: " << errno);
    }
}

bool PhysicalSocketServer::WaitEpoll(int cmsWait) {
    XASSERT(epoll_fd_ != INVALID_SOCKET);
    int64_t tvWait = -1;
    int64_t tvStop = -1;
    if (cmsWait != kForever) {
        tvWait = cmsWait;
        tvStop = Wss::TimeAfter(cmsWait);
    }

    fWait_ = true;
    while (fWait_) {
        // Wait then call handlers as appropriate
        // < 0 means error
        // 0 means timeout
        // > 0 means count of descriptors ready
        IF_LOG("epoll_wait enter, epoll_fd: " << epoll_fd_);
        int n = epoll_wait(epoll_fd_,
                           epoll_events_.data(),
                           epoll_events_.size(),
                           static_cast<int>(tvWait));
        IF_LOG("epoll_wait end nready: " << n);
        if (n < 0) {
            if (errno != EINTR) {
                IF_LOG("epoll error: " << errno);
                return false;
            }
            // Else ignore the error and keep going. If this EINTR was for one of the
            // signals managed by this PhysicalSocketServer, the
            // PosixSignalDeliveryDispatcher will be in the signaled state in the next
            // iteration.
        } else if (n == 0) {
            // If timeout, return success
            return true;
        } else {
            std::vector<uint64_t> should_delete;
            {
            // We have signaled descriptors
            Wss::CritScope cr(&crit_);  //! 会导致这个区间里的Remove、Add、Update都阻塞
            for (int i = 0; i < n; ++i) {
                const epoll_event& event = epoll_events_[i];
                uint64_t key = event.data.u64;
                if (!dispatcher_by_key_.count(key)) {
                    // The dispatcher for this socket no longer exists.
                    IF_LOG("dispatcher_by_key_.count: " << key);
                    continue;
                }
                IF_LOG("dispatcher_by_key_.at: " << key);
                Dispatcher* pdispatcher = dispatcher_by_key_.at(key);
                IF_LOG("dispatcher_by_key_ fd: " << pdispatcher->GetDescriptor());

                bool readable = (event.events & (EPOLLIN | EPOLLPRI));
                bool writable = (event.events & EPOLLOUT);
                bool check_error = (event.events & (EPOLLRDHUP | EPOLLERR | EPOLLHUP));

                ProcessEvents(pdispatcher, readable, writable, check_error);

#if 1
                // 支持 DeleteLater，防止信号中 close 被阻塞
                auto socket = reinterpret_cast<Socket*>(pdispatcher);
                if (socket && socket->ShouldDelete()) {
                    should_delete.push_back(key);
                }
#endif
            }

            } // code block
#if 1
            for (auto sockKey : should_delete) {
                Dispatcher* pdispatcher = dispatcher_by_key_.at(sockKey);
                IF_LOGW("pdispatcher deleted !!!!!!!");
                delete pdispatcher;
            }
#endif
        }

        if (cmsWait != kForever) {
            tvWait = Wss::TimeDiff(tvStop, Wss::TimeMillis());
            if (tvWait <= 0) {
                // Return success on timeout.
                return true;
            }
        }
    }

    return true;
}

// static
void PhysicalSocketServer::ProcessEvents(Dispatcher* dispatcher,
                                         bool readable,
                                         bool writable,
                                         bool check_error) {
    int errcode = 0;
    // TODO(pthatcher): Should we set errcode if getsockopt fails?
    if (check_error) {
        socklen_t len = sizeof(errcode);
        ::getsockopt(dispatcher->GetDescriptor(), SOL_SOCKET, SO_ERROR, &errcode, &len);
    }

    // Most often the socket is writable or readable or both, so make a single
    // virtual call to get requested events
    const uint32_t requested_events = dispatcher->GetRequestedEvents();
    uint32_t ff = 0;

    // Check readable descriptors. If we're waiting on an accept, signal
    // that. Otherwise we're waiting for data, check to see if we're
    // readable or really closed.
    // TODO(pthatcher): Only peek at TCP descriptors.
    if (readable) {
        if (requested_events & DE_ACCEPT) {
            ff |= DE_ACCEPT;
        } else if (errcode || dispatcher->IsDescriptorClosed()) {
            ff |= DE_CLOSE;
        } else {
            ff |= DE_READ;
        }
    }

    // Check writable descriptors. If we're waiting on a connect, detect
    // success versus failure by the reaped error code.
    if (writable) {
        if (requested_events & DE_CONNECT) {
            if (!errcode) {
                ff |= DE_CONNECT;
            } else {
                ff |= DE_CLOSE;
            }
        } else {
            ff |= DE_WRITE;
        }
    }

    // Tell the descriptor about the event.
    // 将所有已经触发过的事件清空或者更新新的事件进去
    if (ff != 0) {
        dispatcher->OnEvent(ff, errcode);
    }
}
