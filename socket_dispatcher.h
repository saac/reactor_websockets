#ifndef SOCKET_DISPATCHER_H
#define SOCKET_DISPATCHER_H
#include "physical_socket.h"

class Dispatcher {
public:
    virtual ~Dispatcher() {}
    virtual uint32_t GetRequestedEvents() = 0;
    virtual void OnEvent(uint32_t ff, int err) = 0;

    virtual int GetDescriptor() = 0;
    virtual bool IsDescriptorClosed() = 0;  // 这个在ssl里面要重写。用sockopt
};

// physical_socket_server.h +218
class SocketDispatcher : public Dispatcher, public PhysicalSocket {
public:
    explicit SocketDispatcher(PhysicalSocketServer* ss);
    SocketDispatcher(SOCKET s, PhysicalSocketServer* ss);
    virtual ~SocketDispatcher() {
        Close();   // 应该是虚的
        IF_LOGW("~SocketDispatcher close success");
    }
    bool Initialize();
    inline virtual bool Create(int type) {
        return Create(AF_INET, type);
    }
    bool Create(int family, int type);
    inline int GetDescriptor() {
        return s_;
    }
    int Close();
    bool IsDescriptorClosed();

    void DeleteLater() override {
        Close();
        m_deleteLater = true;
    }
    bool ShouldDelete() override {
        return m_deleteLater;
    }

    uint32_t GetRequestedEvents() {
        return enabled_events();
    }
    void OnEvent(uint32_t ff, int err);
    inline bool ReadEvent() override {
        // IF_LOG("ReadEvent: " << this->GetRequestedEvents());
        return GetEpollEvents(this->GetRequestedEvents()) & EPOLLIN;
    }
    inline bool WriteEvent() override {
        // IF_LOG("WriteEvent: " << this->GetRequestedEvents());
        return GetEpollEvents(this->GetRequestedEvents()) & EPOLLOUT;
    }
    inline void WantReadWrite(bool read, bool write) override {
        uint8_t ev = 0;
        if (read) {
            IF_LOGW("DE_READ++++++++++");
            ev |= DE_READ;
        }
        if (write) {
            IF_LOGW("DE_WRITE++++++++++");
            ev |= DE_WRITE;
        }
        SetEnabledEvents(ev);   // 其实相当于reset
    }
protected:
    void SetEnabledEvents(uint8_t events) override;
    void EnableEvents(uint8_t events) override;
    void DisableEvents(uint8_t events) override;
    void StartBatchedEventUpdates() {
        // XASSERT(saved_enabled_events_ == -1);
        saved_enabled_events_ = enabled_events();
    }
    void FinishBatchedEventUpdates() {
        //XASSERT(saved_enabled_events_ != -1);
        uint8_t old_events = static_cast<uint8_t>(saved_enabled_events_);
        saved_enabled_events_ = -1;
        MaybeUpdateDispatcher(old_events);  // SocketServer::UpdateEpoll
    }

private:
    void MaybeUpdateDispatcher(uint8_t old_events);
    int saved_enabled_events_ = -1;
    bool m_deleteLater = false;
};

#endif//SOCKET_DISPATCHER_H
