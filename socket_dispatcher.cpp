// #include "physical_socket.hpp"
#include "socket_dispatcher.h"
#include "physical_socket_server.h"
#include "netinet/tcp.h"

// physical_socket_server.h +218
SocketDispatcher::SocketDispatcher(PhysicalSocketServer* ss)
    : PhysicalSocket(ss)
{
}

SocketDispatcher::SocketDispatcher(SOCKET s, PhysicalSocketServer* ss)
    : PhysicalSocket(ss, s)
{
}

bool SocketDispatcher::Initialize() {
    XASSERT(s_ != INVALID_SOCKET);
    // Must be a non-blocking
    fcntl(s_, F_SETFL, fcntl(s_, F_GETFL, 0) | O_NONBLOCK);
    ss_->Add(this);     // from PhysicalSocket
    return true;
}
bool SocketDispatcher::Create(int family, int type) {
    // Change the socket to be non-blocking.
    if (!PhysicalSocket::Create(family, type))
        return false;
    if (!Initialize())
        return false;
    return true;
}

void SocketDispatcher::MaybeUpdateDispatcher(uint8_t old_events) {
    if (GetEpollEvents(PhysicalSocket::enabled_events()) != GetEpollEvents(old_events)
            && saved_enabled_events_ == -1)
    {
        IF_LOG("SocketDispatcher::MaybeUpdateDispatcher");
        ss_->Update(this);
    }
}
void SocketDispatcher::SetEnabledEvents(uint8_t events) {
    uint8_t old_events = enabled_events();
    PhysicalSocket::SetEnabledEvents(events);
    MaybeUpdateDispatcher(old_events);
}

void SocketDispatcher::EnableEvents(uint8_t events) {
    uint8_t old_events = enabled_events();
    PhysicalSocket::EnableEvents(events);   // for dispatcher->GetRequestedEvents()
    MaybeUpdateDispatcher(old_events);
}

void SocketDispatcher::DisableEvents(uint8_t events) {
    uint8_t old_events = enabled_events();
    PhysicalSocket::DisableEvents(events);
    MaybeUpdateDispatcher(old_events);
}

int SocketDispatcher::Close() {
    if (s_ == INVALID_SOCKET)
        return 0;
    if (saved_enabled_events_ != -1) {
        saved_enabled_events_ = 0;
    }
    ss_->Remove(this);
    return PhysicalSocket::Close();
}
// 放到外面去实现
bool SocketDispatcher::IsDescriptorClosed() {
    IF_LOG("SocketDispatcher::IsDescriptorClosed: " << (s_ == INVALID_SOCKET) << " fd:" << s_);
#if 1
    // 单独处理客户端异常关闭的状态
    struct tcp_info info;
    int len = sizeof(info);
    getsockopt(s_, IPPROTO_TCP, TCP_INFO, &info, (socklen_t *)&len);
    bool isclose = info.tcpi_state & (TCP_CLOSE | TCP_CLOSE_WAIT | TCP_CLOSING | TCP_LAST_ACK | TCP_FIN_WAIT1 | TCP_FIN_WAIT2);
    IF_LOG("SocketDispatcher::IsDescriptorClosed res: " << (int)info.tcpi_state);
    // if (isclose) return true;
#endif
    if (udp_) {
        // The MSG_PEEK trick doesn't work for UDP, since (at least in some
        // circumstances) it requires reading an entire UDP packet, which would be
        // bad for performance here. So, just check whether |s_| has been closed,
        // which should be sufficient.
        return s_ == INVALID_SOCKET;
    }
    // We don't have a reliable way of distinguishing end-of-stream
    // from readability.  So test on each readable call.  Is this
    // inefficient?  Probably.
    char ch;
    ssize_t res = ::recv(s_, &ch, 1, MSG_PEEK);
    IF_LOG("SocketDispatcher::IsDescriptorClosed recv: " << res);
    if (res > 0) {
        // Data available, so not closed.
        return false;
    } else if (res == 0) {
        // EOF, so closed.
        return true;
    } else {  // error
        perror("recv");
        IF_LOGW("errno: " << errno);
        switch (errno) {
        // Returned if we've already closed s_.
        case EBADF:
            // Returned during ungraceful peer shutdown.
        case ECONNRESET:
            return true;
            // The normal blocking error; don't log anything.
        case EWOULDBLOCK:
            // Interrupted system call.
        case EINTR:
            return false;
        default:
            // Assume that all other errors are just blocking errors, meaning the
            // connection is still good but we just can't read from it right now.
            // This should only happen when connecting (and at most once), because
            // in all other cases this function is only called if the file
            // descriptor is already known to be in the readable state. However,
            // it's not necessary a problem if we spuriously interpret a
            // "connection lost"-type error as a blocking error, because typically
            // the next recv() will get EOF, so we'll still eventually notice that
            // the socket is closed.
            IF_LOGW("Assuming benign blocking error " << errno);
            if (isclose) return true;
            return false;
        }
    }
}

void SocketDispatcher::OnEvent(uint32_t ff, int err) {
    if ((ff & DE_CONNECT) != 0)
        state_ = CS_CONNECTED;

    if ((ff & DE_CLOSE) != 0)
        state_ = CS_CLOSED;

#if defined(WEBRTC_USE_EPOLL)
    // Remember currently enabled events so we can combine multiple changes
    // into one update call later.
    // The signal handlers might re-enable events disabled here, so we can't
    // keep a list of events to disable at the end of the method. This list
    // would not be updated with the events enabled by the signal handlers.
    StartBatchedEventUpdates();
#endif
    // Make sure we deliver connect/accept first. Otherwise, consumers may see
    // something like a READ followed by a CONNECT, which would be odd.
    if ((ff & DE_CONNECT) != 0) {
        DisableEvents(DE_CONNECT);
        SignalConnectEvent(this);
    }
    if ((ff & DE_ACCEPT) != 0) {
        DisableEvents(DE_ACCEPT);
        SignalReadEvent(this);
    }
    if ((ff & DE_READ) != 0) {
        DisableEvents(DE_READ);
        SignalReadEvent(this);
    }
    if ((ff & DE_WRITE) != 0) {
        DisableEvents(DE_WRITE);
        SignalWriteEvent(this);
    }
    if ((ff & DE_CLOSE) != 0) {
        // The socket is now dead to us, so stop checking it.
        SetEnabledEvents(0);
        IF_LOGW("SignalCloseEvent ................. 1");
        SignalCloseEvent(this, err);
        IF_LOGW("SignalCloseEvent ................. 2");
    }
#if defined(WEBRTC_USE_EPOLL)
    FinishBatchedEventUpdates();
#endif
    IF_LOG("SocketDispatcher::OnEvent end");
}
