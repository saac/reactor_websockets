#ifndef WEBSOCKETS_H
#define WEBSOCKETS_H
#include "common.hpp"
#include "ssl_base.h"
#include "wss_proto.h"
#include <sstream>
#include <map>
#include <queue>
#include "util.h"

using DataArray = std::vector<uint8_t>;
using DataQueue = std::queue<DataArray>;
using HtmlHead = std::map<std::string, std::string>;

class AsyncSocket;
class WssDataCallback {
public:
    WssDataCallback()
    {
    }
    virtual ~WssDataCallback()
    {
    }

    virtual int64_t Read(void *data, int64_t size) = 0;
    virtual int64_t Write(void *data, int64_t size) = 0;
    virtual bool maskEnabled() = 0;  //! client must enable mask!
    virtual void OnTextMessage(const std::string &text) = 0;
    virtual void OnFailedMessage(const std::string &reason) = 0;
    virtual bool isReadyRead() = 0;
    virtual int64_t bytesAvailable() = 0;

public:
    void setSocket(AsyncSocket *socket) {
        m_asyncSocket = socket;
    }
    AsyncSocket *m_asyncSocket;
};

struct WsFrameParser {
public:
    WsFrameParser() { clear(); }

    int m_mask;
    uint8_t m_length;       // payload_len
    enum OpCode m_opCode;
    DataArray m_payload;
    bool m_isFinalFrame;
    bool m_rsv1;
    bool m_rsv2;
    bool m_rsv3;
    bool m_isValid;
    inline void clear()
    {
        // m_closeCode = CloseCodeNormal;
        // m_closeReason.clear();
        m_isFinalFrame = true;
        m_mask = 0;
        m_rsv1 = false;
        m_rsv2 = false;
        m_rsv3 = false;
        m_opCode = OpCodeReservedC;
        m_length = 0;
        m_payload.clear();
        m_isValid = false;
        // 重新初始化到未完成的状态
        m_state.clear();
    }

    // 读取数据帧的状态
    struct ReadState {
        ReadState() { clear(); }
        int64_t bytesRead;
        bool isDone;
        uint64_t dataWaitSize;
        ProcessingState processingState;
        ProcessingState returnState;
        bool hasMask;
        uint64_t payloadLength;

        void clear() {
            bytesRead = 0;
            isDone = false;
            dataWaitSize = 0;
            processingState = PS_READ_HEADER;
            returnState = PS_READ_HEADER;
            hasMask = false;
            payloadLength = 0;
        }
    } m_state;

    inline bool isControlFrame() const
    {
        return (m_opCode & 0x08) == 0x08;
    }
    inline bool isDataFrame() const
    {
        return !isControlFrame();
    }
    bool isFinalFrame() const
    {
        return m_isFinalFrame;
    }
    inline bool isContinuationFrame() const
    {
        return isDataFrame() && (m_opCode == OpCodeContinue);
    }
    OpCode opCode() const
    {
        return m_opCode;
    }
    DataArray payload() const
    {
        return m_payload; // std::move(m_payload);
    }
    inline bool checkValidity()
    {
        if (m_rsv1 || m_rsv2 || m_rsv3) {
            IF_LOG("Rsv field is non-zero");
        } else if (isOpCodeReserved(m_opCode)) {
            IF_LOG("Used reserved opcode");
        } else if (isControlFrame()) {
            if (m_length > 125) {
                IF_LOG("Control frame is larger than 125 bytes");
            } else if (!m_isFinalFrame) {
                IF_LOG("Control frames cannot be fragmented");
            } else {
                m_isValid = true;
            }
        } else {
            m_isValid = true;
        }
        return m_isValid;
    }
    bool isValid() const
    {
        return m_isValid;
    }

public:
    static inline bool isOpCodeReserved(OpCode code)
    {
        return ((code > OpCodeBinary) && (code < OpCodeClose)) || (code > OpCodePong);
    }
};

class WebSockets {
public:
    WebSockets() {}
    ~WebSockets() {}
    // 默认参数以支持普通的 websocket
    void WssInit(WssDataCallback *data_cb, SslBase *base = nullptr) {
        m_sslBase = base;                   // 只有handshake的时候用到
        m_data_cb = data_cb;                // 实际的数据读写走这里
        m_wssState = data_cb->maskEnabled()
                ? WebSockets::WantWrite     // 客户端Wss握手先写
                : WebSockets::WantRead;     // 服务端Wss握手先读
    }
    enum WssState { Unknow, WantWrite, WantRead, WssFinished, WssError };
    WebSockets::WssState WssHandShake(bool isServer = false);
    WebSockets::WssState HandShakeState() { return m_wssState; }
    HtmlHead ParseHtmlHeader(const std::string &html);
    DataArray m_handShakeReply;

    // write
    DataArray getFrameHeader(OpCode opCode,
                             uint64_t payloadLength,
                             uint32_t maskingKey,
                             bool lastFrame);
    int64_t doWriteFrames(const void *data, size_t size, bool isBinary);

    // read
    bool processControlFrame(const WsFrameParser &frame);
    bool process();
    // 从 data_cb 缓存里面读取并解析
    static bool readFrame(WsFrameParser &frame, WssDataCallback *data_cb);

    // 完整数据帧的状态
    struct FrameState {
        FrameState() { clear(); }
        ProcessingState m_processingState;
        bool m_isFinalFrame;
        bool m_isFragmented;
        bool m_isControlFrame;
        uint64_t m_payloadLength;
        OpCode m_opCode;
        uint32_t m_mask;
        bool m_hasMask;

        std::string m_textMessage;
        DataArray m_binaryMessage;

        inline void clear()
        {
            m_processingState = PS_READ_HEADER;
            m_isFinalFrame = false;
            m_isFragmented = false;
            m_isControlFrame = false;
            m_payloadLength = 0;
            m_binaryMessage.clear();
            m_textMessage.clear();
            m_opCode = OpCodeClose;
            m_hasMask = false;
            m_mask = 0;
        }
    } m_frameState;
    WsFrameParser m_frame;  // 设置 setState

private:
    static void mask(uint8_t *payload, uint64_t size, uint32_t maskingKey);
    static void mask(DataArray *payload, uint32_t maskingKey);

private:
    WebSockets::WssState m_wssState = WebSockets::Unknow;
    SslBase *m_sslBase = nullptr;
    WssDataCallback *m_data_cb = nullptr;
};

#endif//WEBSOCKETS_H


