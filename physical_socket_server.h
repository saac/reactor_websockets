#ifndef PHYSICAL_SOCKET_SERVER_H
#define PHYSICAL_SOCKET_SERVER_H
#include "socket_dispatcher.h"

#include <unordered_map>
class Signaler;
class SocketFactory {
public:
    virtual ~SocketFactory() {}

    // Returns a new socket for blocking communication.  The type can be
    // SOCK_DGRAM and SOCK_STREAM.
    //! virtual Socket* CreateSocket(int family, int type) = 0
    // Returns a new socket for nonblocking communication.  The type can be
    // SOCK_DGRAM and SOCK_STREAM.
    virtual AsyncSocket* CreateAsyncSocket(int family, int type) = 0;
};

class SocketServer : public SocketFactory {
public:
    static const int kForever = -1;

    static std::unique_ptr<SocketServer> CreateDefault();
    // When the socket server is installed into a Thread, this function is called
    // to allow the socket server to use the thread's message queue for any
    // messaging that it might need to perform. It is also called with a null
    // argument before the thread is destroyed.
    // virtual void SetMessageQueue(Thread* queue) {}

    // Sleeps until:
    //  1) cms milliseconds have elapsed (unless cms == kForever)
    //  2) WakeUp() is called
    // While sleeping, I/O is performed if process_io is true.
    virtual bool Wait(int cms, bool process_io) = 0;

    // Causes the current wait (if one is in progress) to wake up.
    virtual void WakeUp() = 0;

    // A network binder will bind the created sockets to a network.
    // It is only used in PhysicalSocketServer.
    // void set_network_binder(NetworkBinderInterface* binder) {
    //   network_binder_ = binder;
    // }
    // NetworkBinderInterface* network_binder() const { return network_binder_; }

    // private:
    //NetworkBinderInterface* network_binder_ = nullptr;
};


// class PhysicalSocketServer : public AsyncSocket, public sigslot::has_slots<> {
class PhysicalSocketServer : public SocketServer {
public:
    PhysicalSocketServer();

    virtual ~PhysicalSocketServer();

public:
    AsyncSocket* CreateAsyncSocket(int family, int type) override;

    AsyncSocket* WrapSocket(SOCKET s);

    bool Wait(int cmsWait = -1, bool process_io = true);
    void WakeUp() override;

    void Add(Dispatcher* dispatcher);
    void Remove(Dispatcher* dispatcher);
    void Update(Dispatcher* dispatcher);

    void AddEpoll(Dispatcher* dispatcher, uint64_t key);
    void RemoveEpoll(Dispatcher* dispatcher);
    void UpdateEpoll(Dispatcher* dispatcher, uint64_t key);
    bool WaitEpoll(int cmsWait);    // TODO

    static void ProcessEvents(Dispatcher* dispatcher,
                              bool readable,
                              bool writable,
                              bool check_error);

    SOCKET s_ = INVALID_SOCKET;
    // bool udp_;
    // int family_ = 0;

private:
    const int epoll_fd_ = INVALID_SOCKET;
    uint64_t next_dispatcher_key_ IS_GUARDED_BY(crit_) = 0;

    static constexpr size_t kNumEpollEvents = 128;
    std::array<epoll_event, kNumEpollEvents> epoll_events_;

    bool fWait_ = false;
    static const int kForever = -1;
    Signaler *signal_wakeup_;  // Assigned in constructor only

    Wss::RecursiveCriticalSection crit_;
    std::unordered_map<uint64_t, Dispatcher*> dispatcher_by_key_ IS_GUARDED_BY(crit_);
    std::unordered_map<Dispatcher*, uint64_t> key_by_dispatcher_ IS_GUARDED_BY(crit_);
};

#endif//PHYSICAL_SOCKET_SERVER_H
