#ifndef PHYSICAL_SOCKET_H
#define PHYSICAL_SOCKET_H
#include "async_socket.hpp"

enum DispatcherEvent {
    DE_READ = 0x0001,
    DE_WRITE = 0x0002,
    DE_CONNECT = 0x0004,
    DE_CLOSE = 0x0008,
    DE_ACCEPT = 0x0010,
};

// 只有close是0。另外再分两组。
inline static int GetEpollEvents(uint32_t ff) {
    int events = 0;
    if (ff & (DE_READ | DE_ACCEPT)) {
        events |= EPOLLIN;
    }
    if (ff & (DE_WRITE | DE_CONNECT)) {
        events |= EPOLLOUT;
    }
    return events;
}

class PhysicalSocketServer;
class PhysicalSocket : public AsyncSocket, public sigslot::has_slots<> {
public:
    PhysicalSocket(PhysicalSocketServer* ss, SOCKET s = INVALID_SOCKET);
    ~PhysicalSocket() override {
        Close();
    }

    virtual bool Create(int family, int type);

    // 这里本不是纯虚，但是由 SocketDispatcher 继承了
    virtual void SetEnabledEvents(uint8_t events) {
        enabled_events_ = events;
    }
    virtual void EnableEvents(uint8_t events) {
        IF_LOGW("PhysicalSocket::EnableEvents");
        enabled_events_ |= events;
        IF_LOGW("enabled_events_: " << (int)enabled_events_);
    }
    virtual void DisableEvents(uint8_t events) {
        enabled_events_ &= ~events;
    }
    inline uint8_t enabled_events() const {
        return enabled_events_;
    }
    inline SOCKET DoAccept(SOCKET socket,
                           sockaddr* addr,
                           socklen_t* addrlen)
    {
        return ::accept4(socket, addr, addrlen, SOCK_CLOEXEC);
    }

    inline int Bind(const sockaddr *bind_addr) {
        int err = -1;
        if (bind_addr->sa_family == AF_INET) {
            int opt = true;
            int ret = setsockopt(s_, SOL_SOCKET, SO_REUSEADDR, (const void *)&opt, sizeof(opt));
            err = ::bind(s_, bind_addr, INET_ADDRSTRLEN);
            IF_LOGW("bind s_: " << s_);
        }
        return err;
    }
    inline int Listen(int backlog) override {
        int err = ::listen(s_, backlog);
        //! UpdateLastError();
        if (err == 0) {
            state_ = CS_CONNECTING;
            EnableEvents(DE_ACCEPT);
        }
        IF_LOGW("listen s_: " << s_ << " ret: " << err);
        return err;
    }
    AsyncSocket* Accept(/*SocketAddress*/struct sockaddr_in* out_addr) OVERRIDE(AsyncSocket);

    int Connect(const sockaddr *addr) override;
    int Send(const void* pv, size_t cb) override;
    int Recv(void* buffer, size_t length, int64_t* timestamp) override;
    int Close() override;
    void SetSecurity(SecurityFactory *security) {
        security_ = security;
    }
    ConnState GetState() const override {
        return state_;
    }

    int GetDescriptor() override {
        return s_;
    }

    PhysicalSocketServer* ss_;
    SOCKET s_;
    bool udp_;
    int family_ = 0;

    // mutable webrtc::Mutex mutex_;
    // int error_ RTC_GUARDED_BY(mutex_);
    ConnState state_;
    // AsyncResolver* resolver_;

private:
    int DoConnect(const sockaddr *connect_addr);
    uint8_t enabled_events_ = 0;
    SecurityFactory *security_;
};

#endif//PHYSICAL_SOCKET_H
