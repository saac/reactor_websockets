#include "websockets.h"
#include "common.hpp"   // log
#include "util.h"

WebSockets::WssState WebSockets::WssHandShake(bool isServer) {
    if (m_wssState == WebSockets::WssFinished) {
        return WebSockets::WssFinished;
    }
    if (isServer) {
        const char *ACCEPT_ID = "258EAFA5-E914-47DA-95CA-C5AB0DC85B11";
        if (m_wssState == WebSockets::WantRead) {
            IF_LOG("wss want read");
            char buffer[1024] = {0};
            int nRead = m_sslBase->Read(buffer, sizeof(buffer));
            if (nRead <= 0) {
                IF_LOG("Read error");
                return WantRead;
            }
            // 设置下次进来后的行为
            m_wssState = WantWrite;
            DataArray data(nRead);
            memcpy(data.data(), buffer, nRead);
            std::string html_msg(data.begin(), data.end());
            IF_LOGE("html_header: " << std::endl << html_msg << std::endl);

            HtmlHead headMap = ParseHtmlHeader(html_msg);
            std::vector<std::string> keyList = {
                "Connection",
                "Upgrade",
                "Sec-WebSocket-Version",
                "Sec-WebSocket-Key"
            };
            // 检查必要的字段是否齐全
            if (headMap.size() < keyList.size()) {
                for (std::string key : keyList) {
                    if (headMap.find(key) == headMap.end()) {
                        IF_LOGE("Header info [" << key << "] not received.");
                        continue;
                    }
                    IF_LOGE(key << " " << headMap[key]);
                }
            }
            // 收到计算后发送给客户端：
            // Base64Encode((Sec-WebSocket-Key + ACCEPT_ID).strHexToIntArray)
            // 客户端也应该按照以上方法校验一致才算握手成功
            std::string sha1_sum = Wss::SHA1(headMap["Sec-WebSocket-Key"] + ACCEPT_ID);
            IF_LOG("sha1: " << sha1_sum << " size: " << sha1_sum.size());
            auto strToHexByteArray= [](const std::string &src){
                DataArray sha1_str(src.begin(), src.end());
                DataArray arr;
                while(sha1_str.size()) {
                    std::string byteStr("0x"); // 字符串的insert会自动处理'\0'
                    byteStr.insert(byteStr.end(), sha1_str.begin(), sha1_str.begin()+2);
                    sha1_str.erase(sha1_str.begin(), sha1_str.begin()+2);
                    uint8_t res = strtol(byteStr.data(), nullptr, 16); // 将16进制字符转数值
                    arr.push_back(res);
                }
                return arr;
            };
            DataArray sh1_bytes = strToHexByteArray(sha1_sum);
            DataArray out_b64(64);
            int len = Wss::Base64Encode((char *)sh1_bytes.data(), sh1_bytes.size(), (char *)out_b64.data());
            IF_LOG("Base64Encode: " << len);
            std::string Sec_WebSocket_Accept(out_b64.begin(), out_b64.end());
            IF_LOG("Sec_WebSocket_Accept: " << Sec_WebSocket_Accept);

            // 参考：https://www.cnblogs.com/myzhibie/p/4470065.html
            const char *reply = {
                "HTTP/1.1 101 Switching Protocols\r\n"
                "Date: %s\r\n"
                "Connection: Upgrade\r\n"
                "Server: Wss Server\r\n"
                "Upgrade: websocket\r\n"
                "Sec-WebSocket-Protocol: chat\r\n"
                "Access-Control-Allow-Origin: %s\r\n"               // *
                "Access-Control-Allow-Credentials: %s\r\n"          // true host/false*
                "Sec-WebSocket-Accept: %s\r\n"
                "Access-Control-Allow-Headers: content-type\r\n"
                "Access-Control-Allow-Methods: GET\r\n"
                "\r\n"
            };

            char replyHead[512] = {0};
            // 应该是指定一个服务端的server、port，服务端创建，然后客户端wss去连接
            snprintf(replyHead, sizeof(replyHead), reply,
                     EpUtil::gmtTimeNow("%a, %d %h %Y %T GMT").c_str(),
                     "*",
                     "false",
                     Sec_WebSocket_Accept.c_str());
            m_handShakeReply.resize(strlen(replyHead));
            memcpy(m_handShakeReply.data(), replyHead, m_handShakeReply.size());
            return m_wssState;
        }
        if (m_wssState == WantWrite) {
            IF_LOG("wss want write " << m_handShakeReply.size());
            int nWrite = m_sslBase->Write(m_handShakeReply.data(), m_handShakeReply.size());
            IF_LOG("wss want write res " << nWrite);
            m_wssState = nWrite > 0 ? WssFinished : WssError;
            m_handShakeReply.clear();
            return m_wssState;
        }
    } else {
        if (m_wssState == WebSockets::WantWrite) {
            std::string message;
            struct timeval time;
            gettimeofday(&time, nullptr);
            long now = time.tv_sec*1000 + time.tv_usec/1000;
            int Sec_WebSocket_Version = 13;
            std::stringstream stream;
            stream << Sec_WebSocket_Version << "+" << now;
            std::string key_str;
            stream >> key_str;

            char Sec_WebSocket_Key[25] = {0};
            int bytes = Wss::Base64Encode(key_str.c_str(), key_str.length(), Sec_WebSocket_Key);
            IF_LOG(key_str << " " << bytes << " " << key_str.length());
            IF_LOG(Sec_WebSocket_Key << " " << strlen(Sec_WebSocket_Key));

            const char *request = {
                "GET / HTTP/1.1\r\n"
                "Connection: Upgrade\r\n"
                "Host: %s:%d\r\n"
                "Sec-WebSocket-Key: %s\r\n"
                "Sec-WebSocket-Version: %d\r\n"
                "Upgrade: websocket\r\n"
                "\r\n"
            };

            char requestHead[256] = {0};
            // 应该是指定一个服务端的server、port，服务端创建，然后客户端wss去连接
            snprintf(requestHead, sizeof(requestHead), request,
                     "127.0.0.1",           // socket_client_->Addr().ToString().c_str(),
                     8888,                  // socket_client_->Port(),
                     Sec_WebSocket_Key,
                     Sec_WebSocket_Version);

            int ret = -1;
            ret = m_sslBase->Write(requestHead, strlen(requestHead));
            IF_LOG("Write: " << ret);
            m_wssState = ret > 0 ? WantRead : WantWrite;
            return m_wssState;
        }
        if (m_wssState == WantRead) {
            // buffer 最好要大一些，一次读完，否则再次读取，不好处理 ，会出问题
            char buffer[700] = {0};
            int ret = -1;
            ret = m_sslBase->Read(buffer, sizeof(buffer));
            if (ret == -1 || ret == 0) {
                IF_LOG("Read error");
                return WssError;
            }
            IF_LOG("Read bytes: " << ret);
            IF_LOG(buffer);
            std::string header(buffer);
            if (header.find_first_of("Connection: Upgrade") == std::string::npos) {
                IF_LOGFE("Failed! [Connection: Upgrade] info not found.");
                return WssError;
            }
            m_wssState = ret > 0 ? WssFinished : WssError;
            IF_LOG("WSS HandShake finished! @@@@@@@@@@@@@@@@@@@@");
            IF_LOG("WSS HandShake Res: " << std::endl << buffer << std::endl);
            return m_wssState;
        }
        return WssError;
    }
}
// 将head的内容全部解析成 key-value 的形式方便读取
HtmlHead WebSockets::ParseHtmlHeader(const std::string &html) {
    HtmlHead headMap;

    std::istringstream input;
    input.str(html.c_str());
    for (std::string line; std::getline(input, line, '\n'); ) {
        line.erase(std::remove(line.begin(), line.end(), '\r'),  line.end());
        // 将在\r\n\r\n的地方终止
        if (!line.size()) {
            break;
        }

        std::string::size_type split = line.find_first_of(':');
        // 第一行应该特殊处理一下
        if (split == std::string::npos) {
            IF_LOGW("parser skip: " << line);
            continue;
        }
        std::string key(line.begin(), line.begin() + split);
        std::string value(line.begin() + split + 1, line.end());
        key = Wss::trim(key);
        value = Wss::trim(value);
        headMap[key] = value;
    }
    return headMap;
}

//
// write ============================================================================
//
void WebSockets::mask(uint8_t *payload, uint64_t size, uint32_t maskingKey) {
    const uint8_t mask[] = {
        uint8_t((maskingKey & 0xFF000000u) >> 24),
        uint8_t((maskingKey & 0x00FF0000u) >> 16),
        uint8_t((maskingKey & 0x0000FF00u) >> 8),
        uint8_t((maskingKey & 0x000000FFu))
    };
    int i = 0;
    while (size-- > 0) {
        *payload++ ^= mask[i++ % 4];
    }
}
void WebSockets::mask(DataArray *payload, uint32_t maskingKey)
{
    XASSERT(payload);
    mask(payload->data(), payload->size(), maskingKey);
}

DataArray WebSockets::getFrameHeader(OpCode opCode,
                                     uint64_t payloadLength,
                                     uint32_t maskingKey,
                                     bool lastFrame)
{
    DataArray header;
    bool ok = payloadLength <= 0x7FFFFFFFFFFFFFFFULL;

    if (ok) {
        //FIN, RSV1-3, opcode (RSV-1, RSV-2 and RSV-3 are zero)
        uint8_t byte = static_cast<uint8_t>((opCode & 0x0F) | (lastFrame ? 0x80 : 0x00));
        header.push_back(byte);
        IF_LOGE("xxxxxxxxxxxxxxxx byte1: " << (int)byte);
        byte = 0x00;
        if (maskingKey != 0)
            byte |= 0x80;
        if (payloadLength <= 125) {
            byte |= static_cast<uint8_t>(payloadLength);
            header.push_back(byte);
            IF_LOGE("xxxxxxxxxxxxxxxx byte2: " << (int)byte);
        } else if (payloadLength <= 0xFFFFU) {
            byte |= 126;
            header.push_back(byte);
            uint16_t swapped = Wss::qToBigEndian<uint16_t>(static_cast<uint16_t>(payloadLength));
            uint8_t *p = (uint8_t *)&swapped;
            for (int i = 0; i < 2; ++i) {
                header.push_back(p[i]);
            }
        } else if (payloadLength <= 0x7FFFFFFFFFFFFFFFULL) {
            byte |= 127;
            header.push_back(byte);
            uint64_t swapped = Wss::qToBigEndian<uint64_t>(payloadLength);
            uint8_t *p = (uint8_t *)&swapped;
            for (int i = 0; i < 8; ++i) {
                header.push_back(p[i]);
            }
        }

        if (maskingKey != 0) {
            const uint32_t mask = Wss::qToBigEndian<uint32_t>(maskingKey);
            uint8_t *p = (uint8_t *)&mask;
            for (int i = 0; i < sizeof(mask); ++i) {
                header.push_back(p[i]);
            }
        }
    } else {
        IF_LOGE("WebSocket::getHeader: payload too big!");
        // setErrorString(QStringLiteral("WebSocket::getHeader: payload too big!"));
        // Q_EMIT q_ptr->error(QAbstractSocket::DatagramTooLargeError);
    }

    return header;
}
int64_t WebSockets::doWriteFrames(const void * data, size_t size, bool isBinary) {
    int64_t payloadWritten = 0;
    // 注意这里的链接状态
    if (!m_data_cb || m_wssState != WebSockets::WssFinished)
        return payloadWritten;

    const OpCode firstOpCode = isBinary ? OpCode::OpCodeBinary
                                        : OpCode::OpCodeText;

    int numFrames = size / FRAME_SIZE_IN_BYTES;
    uint8_t *payload = (uint8_t *)data;
    uint64_t sizeLeft = uint64_t(size) % FRAME_SIZE_IN_BYTES;
    if (sizeLeft)
        ++numFrames;

    if (numFrames == 0)
        numFrames = 1;
    uint64_t currentPosition = 0;
    uint64_t bytesLeft = size;

    for (int i = 0; i < numFrames; ++i) {
        uint32_t maskingKey = 0;
        if (m_data_cb->maskEnabled()) {
            maskingKey = Wss::random();
            IF_LOG("mask key: " << maskingKey);
        }

        const bool isLastFrame = (i == (numFrames - 1));
        const bool isFirstFrame = (i == 0);

        const uint64_t size = std::min(bytesLeft, FRAME_SIZE_IN_BYTES);
        const OpCode opcode = isFirstFrame ? firstOpCode
                                           : OpCode::OpCodeContinue;

        // write header
        DataArray sendData = getFrameHeader(opcode, size, maskingKey, isLastFrame);

        // write payload
        if (size > 0) {
            uint8_t *currentData = payload + currentPosition;
            if (m_data_cb->maskEnabled())
                WebSockets::mask(currentData, size, maskingKey);

            DataArray data(size);
            memcpy(data.data(), currentData, size);
            sendData.insert(sendData.end(), data.begin(), data.end());
            payloadWritten += size;
        }
        m_data_cb->Write(sendData.data(), sendData.size());
        currentPosition += size;
        bytesLeft -= size;
    }
    if (payloadWritten != (int64_t)size) {
        IF_LOGE("Bytes written " << payloadWritten << " != " << size);
    }
    return payloadWritten;
}

//
// Read ============================================================================
//
bool WebSockets::processControlFrame(const WsFrameParser &frame)
{
    bool mustStopProcessing = true; //control frames never expect additional frames to be processed
    switch (frame.opCode()) {
    case OpCode::OpCodePing:
        IF_LOG("ping");
        //! Q_EMIT pingReceived(frame.payload());
        break;

    case OpCode::OpCodePong:
        IF_LOG("pong");
        //! Q_EMIT pongReceived(frame.payload());
        break;

    case OpCode::OpCodeClose:
    {
        uint16_t closeCode = CloseCode::CloseCodeNormal;
        // QString closeReason;
        DataArray payload = frame.payload();
        IF_LOG("xxxxxxxxxxxxxxxxxx payload: " << payload.data());
        if (payload.size() == 1) {
            //size is either 0 (no close code and no reason)
            //or >= 2 (at least a close code of 2 bytes)
            closeCode = CloseCode::CloseCodeProtocolError;
            // closeReason = tr("Payload of close frame is too small.");
        } else if (payload.size() > 1) {
            //close frame can have a close code and reason
            closeCode = Wss::qFromBigEndian<uint16_t>(reinterpret_cast<const uint8_t *>(payload.data()));
            if (isCloseCodeValid(closeCode)) {
                closeCode = CloseCode::CloseCodeProtocolError;
                // closeReason = tr("Invalid close code %1 detected.").arg(closeCode);
            } else {
                if (payload.size() > 2) {
                    // QTextCodec *tc = QTextCodec::codecForName(QByteArrayLiteral("UTF-8"));
                    // QTextCodec::ConverterState state(QTextCodec::ConvertInvalidToNull);
                    // closeReason = tc->toUnicode(payload.constData() + 2, payload.size() - 2, &state);
                    // const bool failed = (state.invalidChars != 0) || (state.remainingChars != 0);
                    // if (failed) {
                    //    closeCode = CloseCode::CloseCodeWrongDatatype;
                    //    closeReason = tr("Invalid UTF-8 code encountered.");
                    // }
                }
            }
        }
        IF_LOG("close code: " << closeCode);
        //! Q_EMIT closeReceived(static_cast<QWebSocketProtocol::CloseCode>(closeCode), closeReason);
        break;
    }

    case OpCode::OpCodeContinue:
    case OpCode::OpCodeBinary:
    case OpCode::OpCodeText:
    case OpCode::OpCodeReserved3:
    case OpCode::OpCodeReserved4:
    case OpCode::OpCodeReserved5:
    case OpCode::OpCodeReserved6:
    case OpCode::OpCodeReserved7:
    case OpCode::OpCodeReservedC:
    case OpCode::OpCodeReservedB:
    case OpCode::OpCodeReservedD:
    case OpCode::OpCodeReservedE:
    case OpCode::OpCodeReservedF:
        //do nothing
        //case statements added to make C++ compiler happy
        break;

    default:
        IF_LOG("error");
        //! Q_EMIT errorEncountered(CloseCode::CloseCodeProtocolError,
        //!                        tr("Invalid opcode detected: %1").arg(int(frame.opCode())));
        //do nothing
        break;
    }
    IF_LOG("must stop: " << mustStopProcessing);
    return mustStopProcessing;
}

bool WebSockets::process() {
    //! 这里没有真正的阻塞等待，所以是否有刻度数据优先
    int available = m_data_cb->bytesAvailable();
    bool processDone =false;
    if (available) {
        /*
         * readFrame 相当于 update frame，每次读取解析都产生了一个状态并存储了这个状态
         * 只有符合条件才能继续往下处理，readFrame 后如果需要读取更多的数据就直接返回返回
         * 然后使能读事件，后面的也不用解析了
         */
        if (!WebSockets::readFrame(m_frame, m_data_cb)) {
            IF_LOG("WebSockets::process---------------------------");
            // need read more data;
            return false;
        }
        if (m_frame.isValid()) {
            if (m_frame.isControlFrame()) {
                processDone = processControlFrame(m_frame);
            } else {
                if (!m_frameState.m_isFragmented && m_frame.isContinuationFrame()) {
                    m_frameState.clear();
                    m_data_cb->OnFailedMessage("Received Continuation frame, while there is nothing to continue.");
                    //!TODO m_frame.clear();这些是后来加的，担心会不会影响多帧，应该可以优化
                    m_frame.clear();
                    return true;
                }
                if (m_frameState.m_isFragmented && m_frame.isDataFrame() &&
                        !m_frame.isContinuationFrame())
                {
                    m_frameState.clear();
                    m_data_cb->OnFailedMessage("All data frames after the initial data frame must have opcode 0 (continuation).");
                    m_frame.clear();
                    return true;
                }
                if (!m_frame.isContinuationFrame()) {
                    m_frameState.m_opCode = m_frame.opCode();
                    m_frameState.m_isFragmented = !m_frame.isFinalFrame();
                }
                uint64_t messageLength = (uint64_t)(m_frameState.m_opCode == OpCodeText)
                        ? m_frameState.m_textMessage.size()
                        : m_frameState.m_binaryMessage.size();
                if ((messageLength + uint64_t(m_frame.payload().size())) >
                        MAX_MESSAGE_SIZE_IN_BYTES)
                {
                    m_frameState.clear();
                    m_data_cb->OnFailedMessage("Received message is too big.");
                    m_frame.clear();
                    return true;
                }

                if (m_frameState.m_opCode == OpCodeText) {
                    DataArray temp(m_frame.payload());
                    std::string frameTxt(temp.begin(), temp.end());
                    // if (m_frame.isFinalFrame())
                    //     frameTxt.append("\0");
                    m_frameState.m_textMessage.append(frameTxt);

                    // m_data_cb->OnTextFrame(m_frameState.m_textMessage, frame.isFinalFrame());
                } else {
                    DataArray temp(m_frame.payload());
                    m_frameState.m_binaryMessage.insert(m_frameState.m_binaryMessage.begin(), temp.begin(),temp.end());
                    // m_data_cb->OnBinaryFrame(frame.payload(), frame.isFinalFrame());
                }

                if (m_frame.isFinalFrame()) {
                    processDone = true;
                    if (m_frameState.m_opCode == OpCodeText) {
                        const std::string textMessage(m_frameState.m_textMessage);
                        m_frameState.clear();
                        m_data_cb->OnTextMessage(textMessage);
                        m_frame.clear();
                        //! Q_EMIT textMessageReceived(textMessage);
                    } else {
                        const DataArray binaryMessage(m_frameState.m_binaryMessage);
                        m_frameState.clear();
                        m_frame.clear();
                        // m_data_cb->OnBinaryMessage(binaryMessage);
                        //! Q_EMIT binaryMessageReceived(binaryMessage);
                    }
                }
            }
        } else {
            //! Q_EMIT errorEncountered(frame.closeCode(), frame.closeReason());
            m_data_cb->OnFailedMessage("failed...");
            m_frameState.clear();
            m_frame.clear();
            processDone = true;
        }
    }
    return processDone;
}

// 用成员存储frame，用引用解析。读取结果就两种，读完了一整帧/没有读完
bool WebSockets::readFrame(WsFrameParser &frame, WssDataCallback *data_cb)
{
#define WAIT_FOR_MORE_DATA(dataSizeInBytes) {                   \
    frame.m_state.returnState = frame.m_state.processingState;  \
    frame.m_state.processingState = PS_WAIT_FOR_MORE_DATA;      \
    frame.m_state.dataWaitSize = dataSizeInBytes;               \
}

    while (!frame.m_state.isDone)
    {
        switch (frame.m_state.processingState) {
        case PS_WAIT_FOR_MORE_DATA: {
            //! 需要继续读却没有读到，返回继续读取
            if (!data_cb->isReadyRead()) {
                frame.m_state.processingState = PS_DISPATCH_RESULT;
                frame.m_state.isDone = false;
                return false;
            } else {
                //! 读取到数据后继续之前设置的下一步
                frame.m_state.processingState = frame.m_state.returnState;
            }
        }; break;

        case PS_READ_HEADER:
            if (data_cb->bytesAvailable() >= 2) {
                //FIN, RSV1-3, Opcode
                uint8_t header[2] = {0};
                frame.m_state.bytesRead = data_cb->Read(header, 2);
                frame.m_isFinalFrame = (header[0] & 0x80) != 0;
                frame.m_rsv1 = (header[0] & 0x40);
                frame.m_rsv2 = (header[0] & 0x20);
                frame.m_rsv3 = (header[0] & 0x10);
                frame.m_opCode = static_cast<OpCode>(header[0] & 0x0F);

                //Mask, PayloadLength
                frame.m_state.hasMask = (header[1] & 0x80) != 0;
                frame.m_length = (header[1] & 0x7F);

                IF_LOG("xxxxxxxxxxxx length: " << (int)frame.m_length);

                switch (frame.m_length)
                {
                case 126:
                {
                    frame.m_state.processingState = PS_READ_PAYLOAD_LENGTH;
                    break;
                }
                case 127:
                {
                    frame.m_state.processingState = PS_READ_BIG_PAYLOAD_LENGTH;
                    break;
                }
                default:
                {
                    frame.m_state.payloadLength = frame.m_length;
                    frame.m_state.processingState = frame.m_state.hasMask ? PS_READ_MASK : PS_READ_PAYLOAD;
                    break;
                }
                }
                if (!frame.checkValidity())
                    frame.m_state.processingState = PS_DISPATCH_RESULT;
            } else {
                WAIT_FOR_MORE_DATA(2);
            }
            break;

        case PS_READ_PAYLOAD_LENGTH:
            if (data_cb->bytesAvailable() >= 2) {
                uint8_t length[2] = {0};
                frame.m_state.bytesRead = data_cb->Read(reinterpret_cast<char *>(length), 2);
                if (frame.m_state.bytesRead == -1) {
                    IF_LOGE("Error occurred while reading from the network");
                    frame.m_state.processingState = PS_DISPATCH_RESULT;
                } else {
                    frame.m_state.payloadLength = Wss::qFromBigEndian<uint16_t>(reinterpret_cast<const uint8_t *>(length));
                    if (frame.m_state.payloadLength < 126) {
                        //see http://tools.ietf.org/html/rfc6455#page-28 paragraph 5.2
                        //"in all cases, the minimal number of bytes MUST be used to encode
                        //the length, for example, the length of a 124-byte-long string
                        //can't be encoded as the sequence 126, 0, 124"
                        IF_LOG("Lengths smaller than 126 must be expressed as one byte.");
                        frame.m_state.processingState = PS_DISPATCH_RESULT;
                    } else {
                        frame.m_state.processingState = frame.m_state.hasMask ? PS_READ_MASK : PS_READ_PAYLOAD;
                    }
                }
            } else {
                WAIT_FOR_MORE_DATA(2);
            }
            break;

        case PS_READ_BIG_PAYLOAD_LENGTH:
            if (data_cb->bytesAvailable() >= 8) {
                uint8_t length[8] = {0};
                frame.m_state.bytesRead = data_cb->Read(reinterpret_cast<char *>(length), 8);
                if (frame.m_state.bytesRead < 8) {
                    IF_LOG("Something went wrong during reading from the network.");
                    frame.m_state.processingState = PS_DISPATCH_RESULT;
                } else {
                    //Most significant bit must be set to 0 as
                    //per http://tools.ietf.org/html/rfc6455#section-5.2
                    frame.m_state.payloadLength = Wss::qFromBigEndian<uint64_t>(length);
                    if (frame.m_state.payloadLength & (uint64_t(1) << 63)) {
                        IF_LOG("Highest bit of payload length is not 0.");
                        frame.m_state.processingState = PS_DISPATCH_RESULT;
                    } else if (frame.m_state.payloadLength <= 0xFFFFu) {
                        //see http://tools.ietf.org/html/rfc6455#page-28 paragraph 5.2
                        //"in all cases, the minimal number of bytes MUST be used to encode
                        //the length, for example, the length of a 124-byte-long string
                        //can't be encoded as the sequence 126, 0, 124"
                        IF_LOGE("Lengths smaller than 65536 (2^16), must be expressed as 2 bytes.");
                        frame.m_state.processingState = PS_DISPATCH_RESULT;
                    } else {
                        frame.m_state.processingState = frame.m_state.hasMask ? PS_READ_MASK : PS_READ_PAYLOAD;
                    }
                }
            } else {
                WAIT_FOR_MORE_DATA(8);
            }

            break;

        case PS_READ_MASK:
            if (data_cb->bytesAvailable() >= 4) {
                frame.m_state.bytesRead = data_cb->Read(reinterpret_cast<char *>(&frame.m_mask),
                                                        sizeof(frame.m_mask));
                if (frame.m_state.bytesRead == -1) {
                    IF_LOGE("Error while reading from the network.");
                    frame.m_state.processingState = PS_DISPATCH_RESULT;
                } else {
                    frame.m_mask = Wss::qFromBigEndian(frame.m_mask);
                    frame.m_state.processingState = PS_READ_PAYLOAD;
                }
            } else {
                WAIT_FOR_MORE_DATA(4);
            }
            break;

        case PS_READ_PAYLOAD:
            if (!frame.m_state.payloadLength) {
                frame.m_state.processingState = PS_DISPATCH_RESULT;
            } else if (frame.m_state.payloadLength > MAX_FRAME_SIZE_IN_BYTES) {
                IF_LOGE("Maximum framesize exceeded.");
                frame.m_state.processingState = PS_DISPATCH_RESULT;
            } else {
                uint64_t bytesAvailable = data_cb->bytesAvailable();
                if (bytesAvailable >= frame.m_state.payloadLength) {
                    //! frame.m_payload = buffer->read(payloadLength);
                    // DataArray tmpPyload(frame.m_state.payloadLength);
                    frame.m_payload.resize(frame.m_state.payloadLength);
                    int64_t nRead =data_cb->Read(frame.m_payload.data(), frame.m_state.payloadLength);
                    if (nRead > 0 && nRead < frame.m_payload.size()) {
                        frame.m_payload.resize(nRead);
                    }
                    // frame.m_payload.insert(frame.m_payload.end(), tmpPyload.begin(), tmpPyload.end());
                    // payloadLength can be safely cast to an integer,
                    // because MAX_FRAME_SIZE_IN_BYTES = MAX_INT
                    if (frame.m_payload.size() != int(frame.m_state.payloadLength)) {
                        //some error occurred; refer to the Qt documentation of QIODevice::read()
                        IF_LOGE("Some serious error occurred while reading from the network.");
                        frame.m_state.processingState = PS_DISPATCH_RESULT;
                    } else {
                        if (frame.m_state.hasMask)
                            mask(&frame.m_payload, frame.m_mask);
                        frame.m_state.processingState = PS_DISPATCH_RESULT;
                    }
                } else {
                    //if payload is too big, then this will timeout
                    WAIT_FOR_MORE_DATA(frame.m_state.payloadLength);
                }
            }
            break;

        case PS_DISPATCH_RESULT:
            frame.m_state.processingState = PS_READ_HEADER;
            frame.m_state.isDone = true;
            break;

        default:
            // should not come here
            IF_LOGW("DataProcessor::process: Found invalid state. This should not happen!");
            frame.clear();
            frame.m_state.isDone = true;
            break;
        }	// end switch
    }

    return frame.m_state.isDone;
}
