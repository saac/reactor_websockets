#include "ssl_server.h"
// #include "socket_server.h"

#include "common.hpp"

#include <stdio.h>
#include <stdlib.h>
#include <memory.h>

#include <unistd.h>
#include <signal.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <netinet/tcp.h>    // struct tcp_info

#define CHK_NULL(x)     if ((x)==NULL) exit (1)
#define CHK_ERR(err, s) if ((err)==-1) { perror(s); exit(1); }
#define CHK_SSL(err)    if (( err)==-1) { ERR_print_errors_fp(stderr); fflush(stdout); exit(2); }

// 原文链接：https://blog.csdn.net/wp1603710463/article/details/50365404

bool SslServer::ContextInit() {
    // 初始化OpenSSL环境
    SSL_load_error_strings () ;
    SSLeay_add_ssl_algorithms();
    // 设置SSL协议版本为V2V3自适应
    meth_ = const_cast<SSL_METHOD *>(SSLv23_server_method());
    // 新建SSL上下文句柄
    ctx_ = const_cast<SSL_CTX *>(SSL_CTX_new(meth_));
    if (!ctx_) {
        ERR_print_errors_fp(stderr);
        return false;
    }
    // 设置服务器证书
    if (SSL_CTX_use_certificate_file(ctx_, cert_path_.c_str(), SSL_FILETYPE_PEM) <= 0) {
        ERR_print_errors_fp(stderr);
        return false;
    }
    // 设置服务器私钥
    if (SSL_CTX_use_PrivateKey_file(ctx_, key_path_.c_str(), SSL_FILETYPE_PEM) <= 0) {
        ERR_print_errors_fp(stderr);
        return false;
    }
    // 检查私钥和证书是否匹配
    if (!SSL_CTX_check_private_key(ctx_)) {
        fprintf ( stderr , "Private key does not match the certificate public key.\n");
        return false;
    }
    return true;
}

// 需要对每个accept的fd进行初始化
bool SslServer::SslInit(int accept_fd) {
    IF_LOGW("SslServer::SslInit start");
    if (accept_fd >= 0) {
        // 如何判断一个链接是否有效：
        // https://blog.csdn.net/ht8269/article/details/5499810
        // https://www.cnblogs.com/gwwdq/p/9261615.html
        // tcp.h: https://blog.csdn.net/zhangyu627836411/article/details/80215746
        struct tcp_info info;
        int len = sizeof(info);
        getsockopt(accept_fd, IPPROTO_TCP, TCP_INFO, &info, (socklen_t *)&len);
        if (info.tcpi_state != TCP_ESTABLISHED) {
            IF_LOG("client not connected");
            return false;
        }
    }
    if (!ctx_ && !ContextInit()) {
        IF_LOGW("ContextInit error");
        return false;
    }
    // 新建 accept_fd 对应的 SSL
    ssl_ = SSL_new(ctx_);
    CHK_NULL(ssl_);
    // 设置链接句柄到SSL结构体
    SSL_set_fd(ssl_, accept_fd);
    SSL_set_accept_state(ssl_);
    IF_LOGW("SslServer::SslInit success");
    return true;
}

bool SslServer::SslHandShake() {
    int r = SSL_do_handshake(ssl_);
    IF_LOGW("SSL_do_handshake");

    if (r == 1) {
        m_state = SslBase::SslState::SslConnected;
        want_read_write_ = 0;
        SslInfo();
        IF_LOGW("SSL_do_handshake success");
        return true;
    }
    int err = SSL_get_error(ssl_, r);
    if (err == SSL_ERROR_WANT_WRITE) {
        m_state = SslBase::SslState::SslWantWrite;
        want_read_write_ = err;
        IF_LOGW("SSL_do_handshake WantReadWrite " << err);
    } else if (err == SSL_ERROR_WANT_READ) {
        m_state = SslBase::SslState::SslWantRead;
        want_read_write_ = err;
        IF_LOGW("SSL_do_handshake WantReadWrite " << err);
    } else {
        IF_LOGW("SSL_do_handshake error rw23: " << err);
        CHK_SSL(err);
        return false;
    }
    IF_LOGW("SSL_do_handshake falied");
    return false;
}

void SslServer::SslInfo() {
    // 获得SSL链接用到的算法
    IF_LOG("SSL connection using: " << SSL_get_cipher(ssl_));
    // 获得客户端证书
    X509* client_cert = SSL_get_peer_certificate(ssl_); // X509 结构体，用户保存客户端证书
    if (client_cert) {
        char *str;
        char buf[4096];
        printf("Client certificate:\n");
        str = X509_NAME_oneline(X509_get_subject_name(client_cert), 0, 0);
        CHK_NULL(str);
        IF_LOG("\t subject: " << str);
        OPENSSL_free(str);
        str = X509_NAME_oneline(X509_get_issuer_name(client_cert), 0, 0);
        CHK_NULL(str);
        IF_LOG("\t issuer: " << str);
        OPENSSL_free(str);
        X509_free(client_cert);
    } else {
        IF_LOG("Client does not have certificate.");
    }
}

SslServer::SslServer(const std::string &cert_path,
                     const std::string &key_path)
    : ctx_              (nullptr)
    , ssl_              (nullptr)
    , meth_             (nullptr)
    , cert_path_        (cert_path)
    , key_path_         (key_path)
    , m_state           (SslBase::SslState::SslNotConnected)
    , want_read_write_  (0)
{
}

SslServer::~SslServer() {
    if (!(ssl_ || ctx_)) {
        return;
    }
    SSL_shutdown(ssl_);         // 发送SSL关闭消息
    SSL_free(ssl_);
    SSL_CTX_free(ctx_);
    // fd 由外部传入，外部释放
}

int SslServer::Write(void *data, size_t len) {
    if (ConnectState() != SslBase::SslState::SslConnected) {
        return -1;
    }
    IF_LOG("----------------------------------------------------- SSL write " << len);
    return SSL_write(ssl_, data, len);
}

int SslServer::Read(void *buf, size_t len) {
    if (ConnectState() != SslBase::SslState::SslConnected) {
        return -1;
    }
    return SSL_read(ssl_, buf, len);
}
