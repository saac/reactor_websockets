#ifndef WSS_PROTO_H
#define WSS_PROTO_H

#include <stdint.h>
#include <limits.h>

// maximum size of a frame when sending a message
const uint64_t FRAME_SIZE_IN_BYTES = 512 * 512 * 2;
const uint64_t MAX_FRAME_SIZE_IN_BYTES = INT_MAX - 1;
const uint64_t MAX_MESSAGE_SIZE_IN_BYTES = INT_MAX - 1;

enum OpCode
{
    OpCodeContinue    = 0x0,
    OpCodeText        = 0x1,
    OpCodeBinary      = 0x2,
    OpCodeReserved3   = 0x3,
    OpCodeReserved4   = 0x4,
    OpCodeReserved5   = 0x5,
    OpCodeReserved6   = 0x6,
    OpCodeReserved7   = 0x7,
    OpCodeClose       = 0x8,
    OpCodePing        = 0x9,
    OpCodePong        = 0xA,
    OpCodeReservedB   = 0xB,
    OpCodeReservedC   = 0xC,
    OpCodeReservedD   = 0xD,
    OpCodeReservedE   = 0xE,
    OpCodeReservedF   = 0xF
};

enum ProcessingState
{
    PS_READ_HEADER,
    PS_READ_PAYLOAD_LENGTH,
    PS_READ_BIG_PAYLOAD_LENGTH,
    PS_READ_MASK,
    PS_READ_PAYLOAD,
    PS_DISPATCH_RESULT,

    PS_WAIT_FOR_MORE_DATA
};

enum CloseCode
{
    CloseCodeNormal                 = 1000,
    CloseCodeGoingAway              = 1001,
    CloseCodeProtocolError          = 1002,
    CloseCodeDatatypeNotSupported   = 1003,
    CloseCodeReserved1004           = 1004,
    CloseCodeMissingStatusCode      = 1005,
    CloseCodeAbnormalDisconnection  = 1006,
    CloseCodeWrongDatatype          = 1007,
    CloseCodePolicyViolated         = 1008,
    CloseCodeTooMuchData            = 1009,
    CloseCodeMissingExtension       = 1010,
    CloseCodeBadOperation           = 1011,
    CloseCodeTlsHandshakeFailed     = 1015
};
inline bool isCloseCodeValid(int closeCode)
{
    return  (closeCode > 999) && (closeCode < 5000) &&
            (closeCode != CloseCodeReserved1004) &&          //see RFC6455 7.4.1
            (closeCode != CloseCodeMissingStatusCode) &&
            (closeCode != CloseCodeAbnormalDisconnection) &&
            ((closeCode >= 3000) || (closeCode < 1012));
}
#endif//WSS_PROTO_H
