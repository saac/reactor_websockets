#include "physical_socket.h"
#include "physical_socket_server.h"

PhysicalSocket::PhysicalSocket(PhysicalSocketServer* ss, SOCKET s)
    : ss_(ss)
    , s_(s)
    , state_((s == INVALID_SOCKET) ? CS_CLOSED : CS_CONNECTED)
    , security_(nullptr)
{
    if (s_ != INVALID_SOCKET) {
        IF_LOGW("PhysicalSocket:SetEnabledEvents " << s);
        SetEnabledEvents(DE_READ | DE_WRITE);

        int type = SOCK_STREAM;
        socklen_t len = sizeof(type);
        const int res = getsockopt(s_, SOL_SOCKET, SO_TYPE, (SockOptArg)&type, &len);
        XASSERT(0 == res);
        udp_ = (SOCK_DGRAM == type);
    }
}

bool PhysicalSocket::Create(int family, int type) {
    Close();
    s_ = ::socket(family, type | SOCK_CLOEXEC, 0);
    IF_LOGW("PhysicalSocket::Create s_: " << s_);
    udp_ = (SOCK_DGRAM == type);
    family_ = family;
    //! UpdateLastError();
    if (udp_) {
        SetEnabledEvents(DE_READ | DE_WRITE);
    }
    return s_ != INVALID_SOCKET;
}

AsyncSocket* PhysicalSocket::Accept(/*SocketAddress*/struct sockaddr_in* out_addr) {
    // Always re-subscribe DE_ACCEPT to make sure new incoming connections will
    // trigger an event even if DoAccept returns an error here.
    EnableEvents(DE_ACCEPT);
    //! sockaddr_storage addr_storage;
    //! socklen_t addr_len = sizeof(addr_storage);
    //! sockaddr* addr = reinterpret_cast<sockaddr*>(&addr_storage);
    socklen_t len = INET_ADDRSTRLEN;
    SOCKET s = DoAccept(s_, (struct sockaddr*)out_addr, &len);
    //! UpdateLastError();
    if (s == INVALID_SOCKET)
        return nullptr;
    // TODO
    //! if (out_addr != nullptr)
    //! SocketAddressFromSockAddrStorage(addr_storage, out_addr);

    return ss_->WrapSocket(s);
}

int PhysicalSocket::Connect(const sockaddr *addr) {
    // TODO(pthatcher): Implicit creation is required to reconnect...
    // ...but should we make it more explicit?
    if (state_ != CS_CLOSED) {
        // SetError(EALREADY);
        return SOCKET_ERROR;
    }
    //! if (addr.IsUnresolvedIP()) {

    return DoConnect(addr);
}

int PhysicalSocket::DoConnect(const sockaddr *connect_addr) {
    if ((s_ == INVALID_SOCKET) && !Create(connect_addr->sa_family, SOCK_STREAM)) {
        return SOCKET_ERROR;
    }
    sockaddr_storage addr_storage;
    size_t len = connect_addr->sa_family == AF_INET ? INET_ADDRSTRLEN : INET6_ADDRSTRLEN;
    int err = ::connect(s_, connect_addr, static_cast<int>(len));
    //! UpdateLastError();
    uint8_t events = DE_READ | DE_WRITE;
    if (err == 0) {
        state_ = CS_CONNECTED;
    } else if (IsBlockingError(/*GetError()*/ errno)) {
        state_ = CS_CONNECTING;
        events |= DE_CONNECT;
    } else {
        return SOCKET_ERROR;
    }
    IF_LOGW("PhysicalSocket::DoConnect EnableEvents !!!! " << (int)events);
    EnableEvents(events);
    return 0;
}

int PhysicalSocket::Send(const void* pv, size_t cb) {

    auto DoSend = [this](SOCKET socket, char* buf, int len, int flags) {
        if (security_) {
            IF_LOGW("PhysicalSocket::Send");
            return (ssize_t)security_->Send(buf, len);
        }
        return ::send(socket, buf, len, flags);
    };

    int sent = DoSend(
                s_, const_cast<char*>(reinterpret_cast<const char*>(pv)), static_cast<int>(cb),
            #if defined(WEBRTC_LINUX) && !defined(WEBRTC_ANDROID)
                // Suppress SIGPIPE. Without this, attempting to send on a socket whose
                // other end is closed will result in a SIGPIPE signal being raised to
                // our process, which by default will terminate the process, which we
                // don't want. By specifying this flag, we'll just get the error EPIPE
                // instead and can handle the error gracefully.
                MSG_NOSIGNAL
            #else
                0
            #endif
                );
    // UpdateLastError();
    // MaybeRemapSendError();
    // We have seen minidumps where this may be false.
    XASSERT(sent <= static_cast<int>(cb));
    if ((sent > 0 && sent < static_cast<int>(cb)) ||
            (sent < 0 && IsBlockingError(/*GetError()*/ errno))) {
        EnableEvents(DE_WRITE);
    }
    return sent;
}

int PhysicalSocket::Recv(void* buffer, size_t length, int64_t* timestamp) {
    int received = 0;
    if (security_) {
        received = security_->Recv(buffer, length);
        IF_LOGW("PhysicalSocket::Recv1");
        if (received <= 0) {
            return -1;
        }
    } else {
        received = ::recv(s_, static_cast<char*>(buffer), static_cast<int>(length), 0);
        IF_LOGW("PhysicalSocket::Recv2");
    }
    if ((received == 0) && (length != 0)) {
        // Note: on graceful shutdown, recv can return 0.  In this case, we
        // pretend it is blocking, and then signal close, so that simplifying
        // assumptions can be made about Recv.
        IF_LOG("EOF from socket; deferring close event " << strerror(errno));
        // Must turn this back on so that the select() loop will notice the close
        // event.
#if 0
        //! 读完了就不要再继续读了
        // DeleteLater();
        // SetEnabledEvents(0);
#else
        EnableEvents(DE_READ);
        // SetEnabledEvents(DE_WRITE);
#endif
        //! SetError(EWOULDBLOCK);
        return SOCKET_ERROR;
    }
    if (timestamp) {
        *timestamp = GetSocketRecvTimestamp(s_);
    }
    //! UpdateLastError();
    //! int error = GetError();
    bool success = (received >= 0) || IsBlockingError(errno);
    IF_LOGW("---------------- " << received << "  " << IsBlockingError(errno));
    if (udp_ || success) {
        EnableEvents(DE_READ);
    }
    if (!success && !security_) {
        perror("read");
        IF_LOGW("Error = " << errno);
    }
    return received;
}

int PhysicalSocket::Close() {
    if (s_ == INVALID_SOCKET)
        return 0;
    if (security_) {
        security_->Close();  // 要先关ssl
        security_ = nullptr;
    }

    int err = ::closesocket(s_);
    //! UpdateLastError();
    s_ = INVALID_SOCKET;
    state_ = CS_CLOSED;
    SetEnabledEvents(0);
    // if (resolver_) {
    //   resolver_->Destroy(false);
    //   resolver_ = nullptr;
    // }

    return err;
}
