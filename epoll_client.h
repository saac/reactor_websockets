#ifndef EPOLL_CLIENT_H
#define EPOLL_CLIENT_H
#include "physical_socket_server.h"
#include "util.h"
#include "websockets.h"
#include "common.hpp" //guard

class EpollClient : public WssDataCallback, public sigslot::has_slots<> {
public:
    EpollClient(SslBase::Type type = SslBase::Unknow)
    {
        security_.Create(type);
    }
    ~EpollClient() {}

    Security security_;
    Security *security() {
        return &security_;
    }

    //
    // override WssDataCallback 为 websocket 提供数据的接口
    //
    std::unique_ptr <WebSockets> websockets_;

    DataQueue m_outQueue;
    DataQueue m_inQueue;
    size_t m_inDataSize = 0;
    int64_t Read(void *data, int64_t size) override {
        int readSize = 0;
        IF_LOG("Read m_inDataSize: " << m_inDataSize << " want read: " << size);
        auto guard = Wss::qScopeGuard([&]{
            m_inDataSize -= readSize;
        });
        // 客户端的读，一直是打开的，写是只有需要的时候打开？
        // 这是从缓冲区读取，该有的话一定有，没有的话就等待？
        // 增加异步使能等待的接口？
        // 这里读多次也不影响，每次都使能读取也可以。缓冲区要有尽可能多的数据
        //!来的数据都做了缓存，读到之后解包，因为没读完会一直触发所以读事件一直使能就行
        if (!m_inQueue.size()) {
            return -1;
        }

        DataArray &front = m_inQueue.front();
        if (size < front.size()) {
            memcpy(data, front.data(), size);
            front.erase(front.begin(), front.begin()+size); // read more data
            readSize = size;
            IF_LOG("read more 1 size: " << size);
            return readSize;
        }
        if (size == front.size()) {
            memcpy(data, front.data(), size);
            m_inQueue.pop();
            IF_LOG("pop");
            readSize = size;
            return readSize;
        }
        // 读取到的数据有可能是不够的
        while (size && m_inQueue.size()) {
            IF_LOG("too small");
            int cpySize = front.size() > size ? size : front.size();
            memcpy(data, front.data(), cpySize);
            if (cpySize >= size) {
                m_inQueue.pop();
            } else {
                front.erase(front.begin(), front.begin() + cpySize);
            }
            size -= cpySize;
            readSize += cpySize;
            front = m_inQueue.front();
        }
        return readSize;
    }
    //!TODO：这里只能通知发送一次，一次发不完呢？写完之后继续使能写而不是使能读
    int64_t Write(void *data, int64_t size) override {
        DataArray temp(size);
        memcpy(temp.data(), data, size);
        m_outQueue.emplace(temp);
        m_asyncSocket->WantReadWrite(false, true);
        //! TODO 怎么更好的处理返回值
        return size;
    }
    bool maskEnabled() override { return true; }
    bool isReadyRead() override { return m_inQueue.size(); }
    int64_t bytesAvailable() override {
        IF_LOG("bytesAvailable: " << m_inDataSize << " " << m_inQueue.size());
        return m_inDataSize;
    }
    void OnTextMessage(const std::string &text) override {
        IF_LOG("received, size: " << text.size() << std::endl << text << std::endl);
        sendTextMessage("i am client");
    }
    void OnFailedMessage(const std::string &reason) override {
        IF_LOG("closed reason: " << reason);
    }
    int sendTextMessage(const std::string &msg) {
        return websockets_->doWriteFrames(msg.data(), msg.size(), false);
    }

    void HandShakeUpdate(AsyncSocket *socket) {
        IF_LOGW("HandShake!");
        // 握手成功后直接返回，如果是未连接或者需要读写则下次继续握手
        if (security_.ssl()->ConnectState() == SslBase::SslConnected) {
            IF_LOGW("HandShake Connected!");
            // 因为是非阻塞的，所以默认都关掉？读是应该打开的
            socket->WantReadWrite(false, false);
            return;
        }
        if (security_.ssl()->ConnectState() == SslBase::SslNotConnected) {
            IF_LOGW("HandShake Connected");
            socket->WantReadWrite(false, true);
            return;
        }
        if (security_.ssl()->ConnectState() == SslBase::SslWantRead) {
            socket->WantReadWrite(true, false);
            return;
        }
        if (security_.ssl()->ConnectState() == SslBase::SslWantWrite) {
            socket->WantReadWrite(false, true);
            return;
        }
        return;
    }
    // 客户端连接服务端以后进行读写
    inline void OnClientConnectedRead(AsyncSocket* socket) {
        if (((SocketDispatcher *)socket)->IsDescriptorClosed()) {
            socket->DeleteLater();
            return;
        }
        if (security()->ssl()->ConnectState() != SslBase::SslConnected) {
            security()->ssl()->SslHandShake();
            HandShakeUpdate(socket);
            return;
        }
        if (websockets_->HandShakeState() != WebSockets::WssFinished) {
            WebSockets::WssState state = websockets_->WssHandShake(false);
            if (state == WebSockets::WantWrite) {
                socket->WantReadWrite(false, true);
            }
            if (state == WebSockets::WssFinished) {
                socket->WantReadWrite(false, true);
            }
            return;
        }

        //!TODO: 完善时间戳
        int64_t timestamp = 0;
        char buffer[512] = {0};
        // 不能连续读两次，所以只能buffer足够大，多次读取
        // 如果一次没读完就触发了可写信号呢？根据payload判断，根据协议自身的约定判断
        int nRead = socket->Recv(buffer, sizeof(buffer), &timestamp);
        IF_LOG("----------------------------- nread: " << nRead);
        if (nRead > 0) {
            DataArray temp(nRead);
            mempcpy(temp.data(), buffer, nRead);
            m_inQueue.emplace(temp);
            m_inDataSize += temp.size();
            // 异步处理缓存的 socket 数据，读到了还要继续读，直到完全不可读
            // 返回false是没有读完需要继续读
            bool res = websockets_->process(); // 读取解析数据
            //! processDone == false 说明需要继续读取，但是本来就是偏向于继续读取的状态
            //! 一直关心读事件，优先做数据缓存
            socket->WantReadWrite(true, m_outQueue.size() > 0);
        } else {
            // 没有读到就看有没有可写的，再决定读写事件
            // 这个应该不会执行到，没有数据就不会有读事件被触发
            socket->WantReadWrite(false, m_outQueue.size() > 0);
        }
    }
    //! 把这几个连接状态重构一下
    inline void OnClientConnectedWrite(AsyncSocket* socket) {
        if (((SocketDispatcher *)socket)->IsDescriptorClosed()) {
            socket->DeleteLater();
            return;
        }
        if (security()->ssl()->ConnectState() != SslBase::SslConnected) {
            security()->ssl()->SslHandShake();
            HandShakeUpdate(socket);
            return;
        }
        if (websockets_->HandShakeState() != WebSockets::WssFinished) {
            WebSockets::WssState state = websockets_->WssHandShake(false);
            if (state == WebSockets::WantWrite) {
                socket->WantReadWrite(false, true);
            }
            if (state == WebSockets::WssFinished) {
                socket->WantReadWrite(false, true);
            }
            return;
        }

        static int i = 0;
        i++;
        if (i < 5) {
            IF_LOG("xxxxxxxxxxxxxxxxxxxxxxxx sended");
            std::string data("i am client " + std::to_string(i));
            sendTextMessage(data);
            socket->WantReadWrite(false, true);
            return;
        }

        if (m_outQueue.size()) {
            //! 注意处理ret，防止未写完的情况发生
            DataArray &temp = m_outQueue.front();
            int nWrite = socket->Send(temp.data(), temp.size());
            if (nWrite == temp.size()) {
                m_outQueue.pop();
            } else if (nWrite > 0) {
                // 把写进去的部分擦除了下次继续进来写
                temp.erase(temp.begin(), temp.begin() + nWrite);
            } else {
                //!TODO:完全写错误怎么办？暂时不处理，尝试继续进入写事件，然后epoll_wait结束会判断socket是不是关闭了
            }
            // 如果都有数据则以继续写优先，否则以有数据的优先
            // if (m_outQueue.size() && m_inQueue.size()) {
            //     socket->WantReadWrite(false, true);
            //     return;
            // }
            socket->WantReadWrite(m_outQueue.size(), m_inQueue.size());
            IF_LOG("OnClientConnectedWrite end. nWrite: " << nWrite);
        }
    }
    inline void OnClientConnected(AsyncSocket *socket) {
        if (((SocketDispatcher *)socket)->IsDescriptorClosed()) {
            socket->DeleteLater();
            return;
        }
        // 连上以后读写都不关心了，先初始化 Ssl
        socket->WantReadWrite(false, false);
        bool res = security()->ssl()->Init(socket->GetDescriptor());
        if (!res) {
            IF_LOGW("ssl init error");
            return;
        }
        //!TODO 优化，在ssl都连接就绪以后再初始化 wss
        websockets_.reset(new WebSockets);
        websockets_->WssInit(this, security()->ssl());
        WssDataCallback::setSocket(socket); //! 便于使用 socket->WantReadWrite   TODO:干掉

        socket->SignalWriteEvent.connect(this, &EpollClient::OnClientConnectedWrite);
        socket->SignalReadEvent.connect(this, &EpollClient::OnClientConnectedRead);
        // 客户端握手的时候 read、write 都要打开，ssl 内部应该是主动发起握手请求，所以服务端应该先 read
        socket->WantReadWrite(true, true);

        // EpUtil::Timer::instance()->set(3, [this, socket](){
        //     static int count = 0;
        //     IF_LOG("时间到 " << count++);
        //     std::string data("i am client " + std::to_string(count));
        //     sendTextMessage(data);
        //     socket->WantReadWrite(false, m_outQueue.size());
        // }, false);
    }
    inline void OnClientClosed(AsyncSocket* socket, int err) {
        IF_LOGW("OnClientClosed errno: " << err);
        socket->DeleteLater();
    }
};
#endif//EPOLL_CLIENT_H
