#ifndef SSL_BASE
#define SSL_BASE
#include <stddef.h>

// interface for epoll async_socket.hpp
class SecurityFactory {
public:
    SecurityFactory() {}
    ~SecurityFactory() {}

    virtual int Send(void* pv, size_t cb) = 0;
    virtual int Recv(void* buffer, size_t length) = 0;
    virtual void Close() = 0;
};

class SslBase {
public:
    enum Type { Unknow, Client, Server };
    enum SslState { SslNotConnected, SslConnected, SslWantRead, SslWantWrite };
    Type m_sslType = Unknow;

    SslBase() {}
    virtual ~SslBase() {}

    virtual bool Init(int fd) = 0;
    virtual int Write(void *data, size_t len) = 0;
    virtual int Read(void *buf, size_t len) = 0;
    // virtual void Reset() = 0;

    virtual bool SslHandShake() = 0;
    virtual SslState ConnectState() = 0;
    virtual bool WantRead() = 0;
    virtual bool WantWrite() = 0;
};

// 作为 epoll 的中间层使用
class Security : public SecurityFactory {
public:
    Security() : ssl_ (nullptr) {}
    virtual ~Security() {}
    void Create(SslBase::Type type = SslBase::Unknow);

    int Send(void *pv, size_t cb) override {
        return ssl_->Write(pv, cb);
    }
    int Recv(void* buffer, size_t length) override {
        return ssl_->Read(buffer, length);
    }
    void Close() override {
        // ssl_->shutdown
        if (!ssl_) return;
        delete ssl_;
        ssl_ = nullptr;
    }
    SslBase *ssl() {
        if (!ssl_) {
            Create();
        }
        return ssl_;
    }
    SslBase *ssl_;
    SslBase::Type m_sslType = SslBase::Type::Unknow;
};

#endif//SSL_BASE
