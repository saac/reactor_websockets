#include "ssl_base.h"

#include "ssl_client.h"
#include "ssl_server.h"

void Security::Create(SslBase::Type type) {
    SslBase *ssl = nullptr;
    if (type == SslBase::Unknow) {
        type = m_sslType;
    }
    if (type == SslBase::Server) {
        ssl = new SslServer;
        m_sslType = SslBase::Server;
    } else if (type == SslBase::Client) {
        ssl = new SslClient;
        m_sslType = SslBase::Client;
    } else {
        IF_LOGFE("ssl server type error!");
    }
    ssl_ = ssl;
}


