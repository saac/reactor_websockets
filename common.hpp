#ifndef COMMON_HPP
#define COMMON_HPP

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <sys/time.h>

#include <memory>
#include <vector>
#include <iostream>
#include <algorithm> // std::transform
#include <arpa/inet.h>
#include <openssl/sha.h>

#include <openssl/evp.h>
#include <openssl/x509.h>
#include <openssl/pem.h>
#include <openssl/ssl.h>
#include <openssl/err.h>

#define IF_LOG(X)   std::clog << X << std::endl
#define IF_LOGW(X)  std::clog << X << std::endl
#define IF_LOGE(X)  std::cerr << X << std::endl
#define IF_LOGFE(X) { std::cerr << X << std::endl; /*exit(1);*/ }
#define XASSERT(cond) assert(cond); // false will failed
#define IS_GUARDED_BY(X)
#define OVERRIDE(X) override

namespace Wss {
// 1000ns=1us  1000.000ns=1ms
static const int64_t kNumNanosecsPerSec = INT64_C(1000000000);
inline int64_t TimeNanos() {
    int64_t ticks;
    struct timespec ts;
    clock_gettime(CLOCK_MONOTONIC, &ts);
    ticks = kNumNanosecsPerSec * static_cast<int64_t>(ts.tv_sec) + static_cast<int64_t>(ts.tv_nsec);
    return ticks;
}
static const int64_t kNumMillisecsPerSec = INT64_C(1000);
static const int64_t kNumNanosecsPerMillisec = kNumNanosecsPerSec / kNumMillisecsPerSec;
inline int64_t TimeMillis() {
    return TimeNanos() / kNumNanosecsPerMillisec;
}
inline int64_t TimeDiff(int64_t later, int64_t earlier) {
    return later - earlier;
}
inline int64_t TimeAfter(int64_t elapsed) {
    assert(elapsed >= 0);
    return TimeMillis() + elapsed;
}


inline void SetSocktTimeout(int sock, int sec) {
    struct timeval timeout;
    timeout.tv_sec = sec;
    timeout.tv_usec = 0;
    setsockopt(sock, SOL_SOCKET,SO_RCVTIMEO, (char *)&timeout, sizeof(struct timeval));
}


inline const char* inet_ntop(int af, const void* src, char* dst, socklen_t size) {
    return ::inet_ntop(af, src, dst, size);
}

inline uint32_t HostToNetwork32(uint32_t n) {
    return htobe32(n);
}

template <typename T>
inline void qToUnaligned(const T src, void *dest)
{
    // Using sizeof(T) inside memcpy function produces internal compiler error with
    // MSVC2008/ARM in tst_endian -> use extra indirection to resolve size of T.
    const size_t size = sizeof(T);
    //#if QT_HAS_BUILTIN(__builtin_memcpy)
    __builtin_memcpy
            //#else
            //    memcpy
            //#endif
            (dest, &src, size);
}

/*
 * T qbswap(T source).
 * Changes the byte order of a value from big endian to little endian or vice versa.
 * This function can be used if you are not concerned about alignment issues,
 * and it is therefore a bit more convenient and in most cases more efficient.
*/
template <typename T> constexpr T qbswap(T source);

#  define Q_UINT64_C(c) static_cast<unsigned long long>(c ## ULL) /* unsigned 64 bit constant */

// These definitions are written so that they are recognized by most compilers
// as bswap and replaced with single instruction builtins if available.
template <> inline constexpr uint64_t qbswap<uint64_t>(uint64_t source)
{
    return 0
            | ((source & Q_UINT64_C(0x00000000000000ff)) << 56)
            | ((source & Q_UINT64_C(0x000000000000ff00)) << 40)
            | ((source & Q_UINT64_C(0x0000000000ff0000)) << 24)
            | ((source & Q_UINT64_C(0x00000000ff000000)) << 8)
            | ((source & Q_UINT64_C(0x000000ff00000000)) >> 8)
            | ((source & Q_UINT64_C(0x0000ff0000000000)) >> 24)
            | ((source & Q_UINT64_C(0x00ff000000000000)) >> 40)
            | ((source & Q_UINT64_C(0xff00000000000000)) >> 56);
}

template <> inline constexpr uint32_t qbswap<uint32_t>(uint32_t source)
{
    return 0
            | ((source & 0x000000ff) << 24)
            | ((source & 0x0000ff00) << 8)
            | ((source & 0x00ff0000) >> 8)
            | ((source & 0xff000000) >> 24);
}

template <> inline constexpr uint16_t qbswap<uint16_t>(uint16_t source)
{
    return uint16_t( 0
                     | ((source & 0x00ff) << 8)
                     | ((source & 0xff00) >> 8) );
}

template <> inline constexpr uint8_t qbswap<uint8_t>(uint8_t source)
{
    return source;
}

// ------------------
// signed specializations
template <> inline constexpr int64_t qbswap<int64_t>(int64_t source)
{
    return qbswap<uint64_t>(uint64_t(source));
}

template <> inline constexpr int32_t qbswap<int32_t>(int32_t source)
{
    return qbswap<uint32_t>(uint32_t(source));
}

template <> inline constexpr int16_t qbswap<int16_t>(int16_t source)
{
    return qbswap<uint16_t>(uint16_t(source));
}

template <> inline constexpr int8_t qbswap<int8_t>(int8_t source)
{
    return source;
}
// ------------------
template <typename T> inline void qbswap(const T src, void *dest)
{
    qToUnaligned<T>(qbswap<T>(src), dest);
}
template <typename T>
inline constexpr T qToBigEndian(T source)
{
    return qbswap<T>(source);
}

// +++++++++++++++++++++++++++
template <typename T> inline T qFromUnaligned(const void *src)
{
    T dest;
    const size_t size = sizeof(T);
    //#if QT_HAS_BUILTIN(__builtin_memcpy)
    __builtin_memcpy
            //#else
            //    memcpy
            //#endif
            (&dest, src, size);
    return dest;
}

template <typename T>
inline constexpr T qFromBigEndian(T source)
{ return qbswap<T>(source); }

template <class T>
inline T qFromBigEndian(const void *src)
{
    return qFromBigEndian(qFromUnaligned<T>(src));
}

//template <> inline uint8_t qFromBigEndian<uint8_t>(const void *src)
//{ return static_cast<const uint8_t *>(src)[0]; }
//template <> inline int8_t qFromBigEndian<int8_t>(const void *src)
//{ return static_cast<const int8_t *>(src)[0]; }



inline int Base64Encode(const char *in_str, int in_len, char *out_str) {
    BIO *b64, *bio;
    BUF_MEM *bptr = nullptr;
    size_t size = 0;

    if (in_str == nullptr || out_str == nullptr)
        return -1;

    b64 = BIO_new(BIO_f_base64());
    bio = BIO_new(BIO_s_mem());
    bio = BIO_push(b64, bio);

    BIO_write(bio, in_str, in_len);
    BIO_flush(bio);

    BIO_get_mem_ptr(bio, &bptr);
    memcpy(out_str, bptr->data, bptr->length);
    out_str[bptr->length-1] = '\0';
    size = bptr->length;

    BIO_free(bio);

    return size;
}
inline char * Base64Decode(char *input, int length, bool newLine)
{
    BIO *b64 = nullptr;
    BIO *bmem = nullptr;
    char *buffer = (char *)malloc(length);
    memset(buffer, 0, length);
    b64 = BIO_new(BIO_f_base64());
    if (!newLine) {
        BIO_set_flags(b64, BIO_FLAGS_BASE64_NO_NL);
    }
    bmem = BIO_new_mem_buf(input, length);
    bmem = BIO_push(b64, bmem);
    BIO_read(bmem, buffer, length);
    BIO_free_all(bmem);

    return buffer;
}

// 返回不带结束符的SHA1字符串，由 0~f 表示
inline std::string SHA1(const std::string &str)
{
    auto char2HexChar = [](const unsigned char &x) -> unsigned char
    {
        return x > 9 ? (x - 10 + 'A') : x + '0';
    };

    SHA_CTX c;
    SHA1_Init(&c);
    SHA1_Update(&c, str.c_str(), str.size());
    unsigned char szSha1[SHA_DIGEST_LENGTH] = {0};
    SHA1_Final(szSha1, &c);

    std::string strSha1;
    unsigned char strTmpHex;
    for(int i = 0; i < SHA_DIGEST_LENGTH; ++i)
    {
        strTmpHex = char2HexChar(szSha1[i] / 16);
        strSha1.append(1, strTmpHex);
        strTmpHex = char2HexChar(szSha1[i] % 16);
        strSha1.append(1, strTmpHex);
    }

    std::transform(strSha1.begin(), strSha1.end(), strSha1.begin(), ::tolower);
    return strSha1;
}

inline int32_t random(int start = 0, int end = 0) {
    srand((unsigned)time(nullptr));
    if (start == end && end == 0) {
        return rand();
    }
    return (rand()%(end-start+1)+start);
}



template <typename F>
class ScopeGuard
{
public:
    explicit ScopeGuard(F &&f) noexcept
        : m_func(std::move(f))
    {
    }

    explicit ScopeGuard(const F &f) noexcept
        : m_func(f)
    {
    }

    ScopeGuard(ScopeGuard &&other) noexcept
        : m_func(std::move(other.m_func))
        , m_invoke(std::exchange(other.m_invoke, false))
    {
    }

    ~ScopeGuard() noexcept
    {
        if (m_invoke)
            m_func();
    }

    void dismiss() noexcept
    {
        m_invoke = false;
    }

private:
    // Q_DISABLE_COPY(QScopeGuard)

    F m_func;
    bool m_invoke = true;
};

#ifdef __cpp_deduction_guides
template <typename F> ScopeGuard(F(&)()) -> ScopeGuard<F(*)()>;
#endif

//! [qScopeGuard]
//! https://zhuanlan.zhihu.com/p/21303431
template <typename F>
ScopeGuard<typename std::decay<F>::type> qScopeGuard(F &&f)
{
    return ScopeGuard<typename std::decay<F>::type>(std::forward<F>(f));
}
#if 0
// 这两种写法都OK
auto guard { Wss::qScopeGuard([]{}) };
auto guard = Wss::qScopeGuard([]{});
#endif

// 去除首尾空格字符
inline std::string& trim(std::string &s) {
    if (s.empty()) {
        return s;
    }
    s.erase(0,s.find_first_not_of(" "));
    s.erase(s.find_last_not_of(" ") + 1);
    return s;
}

class ScopedSetTrue {
public:
    ScopedSetTrue(bool* value) : value_(value) {
        assert(!*value_);
        *value_ = true;
    }
    ~ScopedSetTrue() { *value_ = false; }

private:
    bool* value_;
};

class RecursiveCriticalSection {
public:
    RecursiveCriticalSection() {
        pthread_mutexattr_t mutex_attribute;
        pthread_mutexattr_init(&mutex_attribute);
        pthread_mutexattr_settype(&mutex_attribute, PTHREAD_MUTEX_RECURSIVE);

        pthread_mutex_init(&mutex_, &mutex_attribute);
        pthread_mutexattr_destroy(&mutex_attribute);
    }
    ~RecursiveCriticalSection() {
        pthread_mutex_destroy(&mutex_);
    }
    inline void Enter() const {
        pthread_mutex_lock(&mutex_);
    }
    inline bool TryEnter() const {
        if (pthread_mutex_trylock(&mutex_) != 0)
            return false;
        return true;
    }
    inline void Leave() const {
        pthread_mutex_unlock(&mutex_);
    }

private:
    mutable pthread_mutex_t mutex_;
};

class CritScope {
public:
    explicit CritScope(const RecursiveCriticalSection* cs) : cs_(cs) {
        cs_->Enter();
    }
    ~CritScope() {
        cs_->Leave();
    }

private:
    const RecursiveCriticalSection* const cs_;
    //! RTC_DISALLOW_COPY_AND_ASSIGN(CritScope);
};




class MutexImpl final {
public:
    MutexImpl() {
        pthread_mutexattr_t mutex_attribute;
        pthread_mutexattr_init(&mutex_attribute);
        pthread_mutex_init(&mutex_, &mutex_attribute);
        pthread_mutexattr_destroy(&mutex_attribute);
    }
    MutexImpl(const MutexImpl&) = delete;
    MutexImpl& operator=(const MutexImpl&) = delete;
    ~MutexImpl() { pthread_mutex_destroy(&mutex_); }

    inline void Lock() { pthread_mutex_lock(&mutex_); }
    inline bool TryLock() { return pthread_mutex_trylock(&mutex_) == 0; }
    inline void Unlock() { pthread_mutex_unlock(&mutex_); }

private:
    pthread_mutex_t mutex_;
};
class Mutex final {
public:
    Mutex() = default;
    Mutex(const Mutex&) = delete;
    Mutex& operator=(const Mutex&) = delete;

    inline void Lock() {
        impl_.Lock();
    }
    inline bool TryLock() {
        return impl_.TryLock();
    }
    inline void Unlock() {
        impl_.Unlock();
    }

private:
    MutexImpl impl_;
};

inline bool setPortReuse(int fd) {
    int opt = true;
    int ret = setsockopt(fd, SOL_SOCKET, SO_REUSEADDR, (const void *)&opt, sizeof(opt));
    return ret == 0;
}

}; // Wss
#endif//COMMON_HPP
