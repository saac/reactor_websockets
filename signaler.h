#ifndef SIGNALER_H
#define SIGNALER_H
#include "socket_dispatcher.h"
#include "physical_socket_server.h"

class Signaler : public Dispatcher {
public:
    Signaler(PhysicalSocketServer* ss, bool& flag_to_clear)
        : ss_(ss),
          afd_([] {
        std::array<int, 2> afd = {-1, -1};

        if (pipe(afd.data()) < 0) {
            IF_LOG("pipe failed");
        }
        IF_LOG("pipe fd: " << afd[0] << " " << afd[1]);
        return afd;
    }()),
          fSignaled_(false),
          flag_to_clear_(flag_to_clear)
    {
        ss_->Add(this);
        IF_LOG("Signaler created: " << this);
    }

    ~Signaler() override {
        IF_LOG("~Signaler xxxxxxxxxxxxx start" << this);
        ss_->Remove(this);
        close(afd_[0]);
        close(afd_[1]);
        IF_LOG("~Signaler xxxxxxxxxxxxx end" << this);
    }

    inline virtual void Signal() {
        mutex_.Lock();
        if (!fSignaled_) {
            const uint8_t b[1] = {0};
            const ssize_t res = write(afd_[1], b, sizeof(b));
            XASSERT(1 == res);
            fSignaled_ = true;
        }
        mutex_.Unlock();
    }

    inline uint32_t GetRequestedEvents() override { return DE_READ; }

    inline void OnEvent(uint32_t ff, int err) override {
        // It is not possible to perfectly emulate an auto-resetting event with
        // pipes.  This simulates it by resetting before the event is handled.

        mutex_.Lock();
        if (fSignaled_) {
            uint8_t b[4];  // Allow for reading more than 1 byte, but expect 1.
            const ssize_t res = read(afd_[0], b, sizeof(b));
            XASSERT(1 == res);
            fSignaled_ = false;
        }
        flag_to_clear_ = false;
        mutex_.Unlock();
        IF_LOG("Signaler::OnEvent");
    }

    inline int GetDescriptor() override { return afd_[0]; }

    inline bool IsDescriptorClosed() override { return false; }

private:
    PhysicalSocketServer* const ss_;
    const std::array<int, 2> afd_;
    bool fSignaled_ IS_GUARDED_BY(mutex_);
    Wss::Mutex mutex_;
    bool& flag_to_clear_;
};


#endif//SIGNALER_H
