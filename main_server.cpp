#include "epoll_server.h"

class ServerAcceptor: public sigslot::has_slots<> {
public:
    ServerAcceptor()
    {}
    ~ServerAcceptor() {}
    inline void OnServerConnectRead(AsyncSocket* socket) {
        // 不同的accept对应不同的security
        IF_LOGW("OnServerAccept::OnServerConnectRead");
        struct sockaddr_in accept_addr;
        AsyncSocket *accept_sock = socket->Accept(&accept_addr);
        // accept 后啥也不干，先初始化
        accept_sock->WantReadWrite(false, false);
        IF_LOG("socket Accept " << accept_sock->GetDescriptor());
        EpollServer *accept_cb_ = new EpollServer(SslBase::Server);
        if (!accept_cb_->security()->ssl()->Init(accept_sock->GetDescriptor())) {
            IF_LOG("SslInit error, close socket...");
            accept_sock->DeleteLater();         // 这里最好能将accept_cb_也释放掉
            delete accept_cb_;
            return;
        }
        IF_LOGW("ssl()->Init success");
        accept_cb_->setSocket(accept_sock);
        accept_sock->SetSecurity(accept_cb_->security());   // disable events
        accept_sock->SignalReadEvent.connect(accept_cb_, &EpollServer::OnServerAcceptRead);
        accept_sock->SignalWriteEvent.connect(accept_cb_, &EpollServer::OnServerAcceptWrite);
        accept_sock->SignalCloseEvent.connect(accept_cb_, &EpollServer::OnServerClose);
        // 握手过程，服务端是先使能读，客户端是读写同时是使能
        accept_sock->WantReadWrite(true, false);
    }
};

int main(int argc, char *argv[]) {
    std::unique_ptr<PhysicalSocketServer> server(new PhysicalSocketServer);
    std::unique_ptr<AsyncSocket> socket(server->CreateAsyncSocket(AF_INET, SOCK_STREAM));
    if (!socket) {
        IF_LOG("Create failed");
        return -1;
    }
    struct sockaddr_in sockaddr_v4;
    sockaddr_v4.sin_family = AF_INET;
    sockaddr_v4.sin_port = htons(8888);
    sockaddr_v4.sin_addr.s_addr = inet_addr("0.0.0.0");
    int ret  = 0;
    ret = socket->Bind((sockaddr *)&sockaddr_v4);
    XASSERT(ret == 0);
    ret = socket->Listen(5);
    XASSERT(ret == 0);

    ServerAcceptor acceptor;
    socket->SignalReadEvent.connect(&acceptor, &ServerAcceptor::OnServerConnectRead);
    // 服务端几乎永不到这个
    // socket->SignalCloseEvent.connect(&acceptor, &ServerAcceptor::OnServerClose);
    server->Wait(-1, true);
    return 0;
}
