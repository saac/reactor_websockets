#ifndef WSS_CLIENT_H
#define WSS_CLIENT_H
#include "ssl_client.h"
#include "wss_protocol.h"

// #include "socket_client.h"

#include <memory>

class WssClient : public SocketDataCallback, public WsProtocol {
public:
    WssClient();
    ~WssClient();

    // bool WssInit();

    int64_t Read(void *data, int64_t size) override;
    int64_t Write(void *data, int64_t size) override;
    bool maskEnabled () override;
    void OnTextFrame(const std::string &text, bool isFinalFrame) override;
    void OnTextMessage(const std::string &text) override;
    void OnBinaryFrame(const DataArray &data, bool isFinalFrame);
    void OnBinaryMessage(const DataArray &data) override;
    void OnFailedMessage(const std::string &reason) override;

    // bool Handshark();

private:
    // std::unique_ptr<SslClient>      ssl_client_;
    // std::unique_ptr<SocketClient>   socket_client_;
};

//static const char kHeaderTerminator[] = "\r\n\r\n";
//static const int kHeaderTerminatorLength = sizeof(kHeaderTerminator) - 1;
//static const char kCrossOriginAllowHeaders[] =
//    "Access-Control-Allow-Origin: *\r\n"
//    "Access-Control-Allow-Credentials: true\r\n"
//    "Access-Control-Allow-Methods: POST, GET, OPTIONS\r\n"
//    "Access-Control-Allow-Headers: Content-Type, "
//    "Content-Length, Connection, Cache-Control\r\n"
//    "Access-Control-Expose-Headers: Content-Length\r\n";

#endif// WSS_CLIENT_H
