#include "epoll_client.h"

int main(int argc, char *argv[]) {
    std::unique_ptr<PhysicalSocketServer> server(new PhysicalSocketServer);
    std::unique_ptr<AsyncSocket> socket(server->CreateAsyncSocket(AF_INET, SOCK_STREAM));

    // 先连接信号，避免发起 Connect 后接收不到信号
    EpollClient c(SslBase::Client);
    socket->SetSecurity(c.security());
    socket->SignalConnectEvent.connect(&c, &EpollClient::OnClientConnected);
    socket->SignalCloseEvent.connect(&c, &EpollClient::OnClientClosed);

    struct sockaddr_in sockaddr_v4;
    sockaddr_v4.sin_family = AF_INET;
    sockaddr_v4.sin_port = htons(8888);
    sockaddr_v4.sin_addr.s_addr = inet_addr("127.0.0.1");
    const struct sockaddr *server_sock = reinterpret_cast<struct sockaddr *>(&sockaddr_v4);

    // connect will enable EPOLLIN/EPOLLOUT
    int ret = socket->Connect(server_sock);
    XASSERT(ret == 0);
    server->Wait(-1, true);
    return 0;
}
