#ifndef EP_COMMON_H
#define EP_COMMON_H
#include "common.hpp"
#include "sigslot.h"
#include <unistd.h> // close
#include <sys/epoll.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <sys/ioctl.h>

typedef int SOCKET;
typedef void* SockOptArg;

#define INVALID_SOCKET (-1)
#define SOCKET_ERROR (-1)
#define __FD_SETSIZE		1024
#define closesocket(s) close(s)
#define WEBRTC_POSIX
#define WEBRTC_USE_EPOLL

static const int64_t kNumMicrosecsPerSec = INT64_C(1000000);

inline int64_t GetSocketRecvTimestamp(int socket) {
    struct timeval tv_ioctl;
    int ret = ioctl(socket, SIOCGSTAMP, &tv_ioctl);
    if (ret != 0)
        return -1;
    int64_t timestamp = kNumMicrosecsPerSec * static_cast<int64_t>(tv_ioctl.tv_sec) + static_cast<int64_t>(tv_ioctl.tv_usec);
    return timestamp;
}
inline bool IsBlockingError(int e) {
  return (e == EWOULDBLOCK) || (e == EAGAIN) || (e == EINPROGRESS);
}

#endif//EP_COMMON_H
