#ifndef ASYNC_SOCKET_HPP
#define ASYNC_SOCKET_HPP

#include "ep_common.h"
#include "ssl_base.h"
#include "ipaddress.h"

// TODO AsyncSocket -> Socket
class Socket {
public:
    virtual ~Socket() {}

    virtual int Bind(const sockaddr *bind_addr) = 0;
    virtual int Listen(int backlog) = 0;
    virtual int Connect(const sockaddr *addr) = 0;
    virtual Socket* Accept(struct sockaddr_in* paddr) = 0;
    virtual int Send(const void* pv, size_t cb) = 0;
    virtual int Recv(void* buffer, size_t length, int64_t* timestamp) = 0;
    virtual int Close() = 0;
    virtual bool ReadEvent() { IF_LOG("Error, ReadEvent"); exit(-1); }
    virtual bool WriteEvent() { IF_LOG("Error, WriteEvent"); exit(-1); }
    virtual void WantReadWrite(bool read, bool write) = 0;
    virtual void SetSecurity(SecurityFactory *security) = 0;
    virtual int GetDescriptor() = 0;

    virtual void DeleteLater() = 0;
    virtual bool ShouldDelete() = 0;

    enum ConnState { CS_CLOSED, CS_CONNECTING, CS_CONNECTED };
    virtual ConnState GetState() const = 0;
};

class AsyncSocket : public Socket {
public:
    AsyncSocket() {}
    virtual ~AsyncSocket() {
        IF_LOGW("~AsyncSocket");
    }

    // 此处因为返回值不同
    AsyncSocket* Accept(struct sockaddr_in* paddr) override = 0;

    // SignalReadEvent and SignalWriteEvent use multi_threaded_local to allow
    // access concurrently from different thread.
    // For example SignalReadEvent::connect will be called in AsyncUDPSocket ctor
    // but at the same time the SocketDispatcher maybe signaling the read event.
    // ready to read
    sigslot::signal1<AsyncSocket*, sigslot::multi_threaded_local> SignalReadEvent;
    // ready to write
    sigslot::signal1<AsyncSocket*, sigslot::multi_threaded_local> SignalWriteEvent;
    // connected
    sigslot::signal1<AsyncSocket*> SignalConnectEvent;
    // closed
    sigslot::signal2<AsyncSocket*, int> SignalCloseEvent;
};

#endif//ASYNC_SOCKET_HPP
