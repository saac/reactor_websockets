#!/bin/bash
# https://www.thinbug.com/q/54402673

echo " 配置文件中已经有默认值了，shell交互时一路回车就行"

cat << EOF > ca.conf
[ req ]
default_bits       = 4096
distinguished_name = req_distinguished_name

[ req_distinguished_name ]
countryName                 = Country Name (2 letter code)
countryName_default         = CN
stateOrProvinceName         = State or Province Name (full name)
stateOrProvinceName_default = JiangSu
localityName                = Locality Name (eg, city)
localityName_default        = NanJing
organizationName            = Organization Name (eg, company)
organizationName_default    = Sheld
commonName                  = Common Name (e.g. server FQDN or YOUR name)
commonName_max              = 64
commonName_default          = Ted CA Test
EOF

# 生成ca密钥 ca.key
openssl genrsa -out ca.key 4096

# 生成ca证书签发请求，ca.csr
yes "" | openssl req \
  -new \
  -sha256 \
  -out ca.csr \
  -key ca.key \
  -config ca.conf

# 生成ca根证书, ca.crt
openssl x509 \
    -req \
    -days 3650 \
    -in ca.csr \
    -signkey ca.key \
    -out ca.crt

cat << EOF > server.conf
[ req ]
default_bits       = 2048
distinguished_name = req_distinguished_name
req_extensions     = req_ext

[ req_distinguished_name ]
countryName                 = Country Name (2 letter code)
countryName_default         = CN
stateOrProvinceName         = State or Province Name (full name)
stateOrProvinceName_default = JiangSu
localityName                = Locality Name (eg, city)
localityName_default        = NanJing
organizationName            = Organization Name (eg, company)
organizationName_default    = Sheld
commonName                  = Common Name (e.g. server FQDN or YOUR name)
commonName_max              = 64
commonName_default          = www.ted2018.com

[ req_ext ]
subjectAltName = @alt_names

[alt_names]
DNS.1   = www.ted-go.com
DNS.2   = www.ted2018.com
IP      = 192.168.93.145
EOF

# 生成秘钥，得到 server.key
openssl genrsa -out server.key 2048

# 生成证书签发请求，得到server.csr
yes "" | openssl req \
  -new \
  -sha256 \
  -out server.csr \
  -key server.key \
  -config server.conf

# 用CA证书生成终端用户证书，得到server.crt
openssl x509 \
  -req \
  -days 3650 \
  -CA ca.crt \
  -CAkey ca.key \
  -CAcreateserial \
  -in server.csr \
  -out server.crt \
  -extensions req_ext \
  -extfile server.conf


