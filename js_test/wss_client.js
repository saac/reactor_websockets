// 官方文档： https://nodejs.org/api/https.html#https_https_get_options_callback
// http://cn.voidcc.com/question/p-ssnpbnxz-ca.html
// Error: self-signed certificate:  
// http://cn.voidcc.com/question/p-ksgerniw-bhc.html
// 最好的js实例：https://www.cnblogs.com/axes/p/4514199.html

// client来源：https://www.cnblogs.com/zhanghuizong/p/8668769.html
// server的实现：见上面几篇文章。最好使用 simple_server.js 的实现
// 这篇 nodejs 的也很详细：https://www.asni.cn/2152
// 关于请求头：https://blog.csdn.net/yangbo10086/article/details/84729551
// GMT时间与CST时间：https://www.cnblogs.com/kaituorensheng/p/3922919.html

const https   = require('https');
const crypto = require('crypto');
const url    = require('url');
 
// var serverUrl       = url.parse('wss://localhost:8888');
var serverUrl       = url.parse('wss://192.168.31.184:8888');
var protocolVersion = 13;
var keyIn           = protocolVersion + '-' + Date.now();
var key             = new Buffer.alloc(keyIn.length, keyIn).toString('base64');
console.log(keyIn)
// 版本、时间太重要了，server端是这么验证的：
// var shasum          = crypto.createHash('sha1');
// shasum.update(key + '258EAFA5-E914-47DA-95CA-C5AB0DC85B11');
// var expectedServerKey = shasum.digest('base64');
 
var headerHost     = serverUrl.host;
var requestOptions = {
    port   : serverUrl.port,
    host   : serverUrl.hostname,
    headers: {
        'Connection'              : 'Upgrade',
        'Upgrade'                 : 'websocket',
        'Host'                    : '192.168.31.184', //headerHost,
        'Sec-WebSocket-Version'   : protocolVersion,
        'Sec-WebSocket-Key'       : key,
        // 'Sec-WebSocket-Extensions': '', // permessage-deflate; client_max_window_bits
        'path'                    : '/'
    }
    , rejectUnauthorized: false

};
var req            = https.request(requestOptions);
req.once('upgrade', function (res, socket, upgradeHead) {
    var headers = res.headers;
    console.log('upgrade: ', headers);
    // console.log('upgrade: ', res);
    socket.setTimeout(0);
    socket.setNoDelay(true);
 
    socket.on('data', function (data) {
        console.log('..............', data.toString('utf8'));
    });
    // socket.end();
});
req.end();




// server端是如何回复的：
// server.on('upgrade', function (req, socket, upgradeHead) {
//     console.log('upgrade');
//     var key = req.headers['sec-websocket-key'];
//     key = crypto.createHash("sha1").update(key + "258EAFA5-E914-47DA-95CA-C5AB0DC85B11").digest("base64");
//     var headers = [
//         'HTTP/1.1 101 Switching Protocols',
//         'Upgrade: websocket',
//         'Connection: Upgrade',
//         'Sec-WebSocket-Accept: ' + key
//     ];
//     socket.setNoDelay(true);
//     socket.write(headers.join("\r\n") + "\r\n\r\n", 'ascii');
//     socket.end();
//     var wss_sock = createWebSocket();
// });
