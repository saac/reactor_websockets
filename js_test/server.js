const fs = require('fs');
const hts = require('https');
const WebSocketServer = require('ws').Server;

const cfg = {
    port:     8888,
    host:     '192.168.31.184',
    sslKey:   fs.readFileSync('./key.pem'),
    sslCert:  fs.readFileSync('./cert.pem'),
    // htmlPage: fs.readFileSync("./simple_client.html")
};

// var express = require('express'),
// app = express(),
// server = require('https').createServer(app);

// 创建request请求监听器
const processRequest = (req, res) => {
    res.writeHead(200);
    // res.end(cfg.htmlPage);
    res.end();
};

const server = hts.createServer({ key: cfg.sslKey, cert: cfg.sslCert }, processRequest)
    .listen(cfg.port, ()=>{ console.log('HTTPS Server is running on: https://%s:%s', cfg.host, cfg.port);
 });

// server.listen(3000);

// app.get('/',  function(req, res) {
//     res.sendfile(__dirname + '/simple_client.html');
// });

// var WebSocketServer = require('ws').Server,
// wss =  new WebSocketServer({server: server});
const wss = new WebSocketServer({ server: server });  // server : httpsserver

//  存储socket的数组，这里只能有2个socket，每次测试需要重启，否则会出错
var wsc = [],
index = 1;

//  有socket连入
wss.on('connection', function(ws) {
    console.log('connection');

    //  将socket存入数组
    wsc.push(ws);

    //  记下对方socket在数组中的下标，因为这个测试程序只允许2个socket
    //  所以第一个连入的socket存入0，第二个连入的就是存入1
    //  otherIndex就反着来，第一个socket的otherIndex下标为1，第二个socket的otherIndex下标为0
    var otherIndex = index--,
    desc =  null;

    if (otherIndex == 1) {
        desc = 'recv';
    } else {
        desc = 'call';
    }

     //  转发收到的消息
    ws.on('close',  function(message) {
        // wsc[0].close();
        wsc.pop();
    });
    let add_value = 0;
    ws.on('message',  function(message) {
        console.log('msg received: ' + message);

        add_value++;
        // var reply = Buffer.from(add_value.toString()).toString();
        wsc[0].send(add_value.toString(),  function (error) {
            console.log('send number' + add_value);
            if (error != null)
                console.log('send error' + error);
            return;
        });
        return;
    });
});
