用该命令测试服务端信息
openssl s_client -connect 192.168.31.184:8888

DATE=`date  "+%s%N" | cut -b 1-14`
HOST="192.168.31.184"
PORT="8888"
DATE_STRING="13+${DATE}"

GUID='258EAFA5-E914-47DA-95CA-C5AB0DC85B11'
KEY=$(echo -n "${DATE_STRING}" | base64)

# 一步搞定：
# 计算 base64、sha1 都不能加换行符
KEY_GEN=$(echo -n $(echo -n \"13-$(date +%s%N | cut -b 1-14)${GUID}\" | base64) | sha1sum) && echo "GET / HTTP/1.1
Connection: Upgrade
Host: ${HOST}:${PORT}
Sec-WebSocket-Key: ${KEY_GEN}
Sec-WebSocket-Version: 13
Upgrade: websocket

" | openssl -verify false s_client -connect 192.168.31.184:8888


openssl verify -verbose -x509_strict -CAfile ca.pem certificate.pem

ca证书验证错误：
    https://www.thinbug.com/q/54402673
生成自签名证书：
    https://www.cnblogs.com/nidey/p/9041960.html


