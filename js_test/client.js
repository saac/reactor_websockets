const https   = require('https');
const crypto = require('crypto');
const url    = require('url');

var serverUrl       = url.parse('wss://192.168.31.184:8888');
var protocolVersion = 13;
var keyIn           = protocolVersion + '-' + Date.now();
var key             = new Buffer.alloc(keyIn.length, keyIn).toString('base64');
console.log(keyIn)
 
var headerHost     = serverUrl.host;
var requestOptions = {
    port   : serverUrl.port,
    host   : serverUrl.hostname,
    headers: {
        'Connection'              : 'Upgrade',
        'Upgrade'                 : 'websocket',
        'Host'                    : '192.168.31.184', 
        'Sec-WebSocket-Version'   : protocolVersion,
        'Sec-WebSocket-Key'       : key,
        // 'Sec-WebSocket-Extensions': '', // permessage-deflate; client_max_window_bits
        'path'                    : '/'
    }
    , rejectUnauthorized: false

};
var req            = https.request(requestOptions);
req.once('upgrade', function (res, socket, upgradeHead) {
    var headers = res.headers;
    console.log('upgrade: ', headers);
    socket.setTimeout(0);
    socket.setNoDelay(true);
 
    socket.on('data', function (data) {
        console.log('..............', data.toString('utf8'));
    });
    socket.write("xxxxxxxxxxxxxxxxx");
    // socket.end();
});
req.end();


