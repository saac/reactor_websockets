#include "ssl_client.h"
//#include "socket_client.h"
#include <unistd.h> // STDOUT_FILENO

#define CHK_NULL(x) if ((x) == nullptr) exit(1)
#define CHK_ERR(err,s) if ((err) == -1) { perror(s); exit(1); }
#define CHK_SSL(err) if ((err) == -1) { ERR_print_errors_fp(stderr); exit(2); }

// 原文链接：https://blog.csdn.net/wp1603710463/article/details/50365404

// 在connect之后init，因为要SSL_set_connect_state
bool SslClient::SslInit(int fd) {
    if (fd > 0) {
        // 如何判断一个链接是否有效：
        // https://blog.csdn.net/ht8269/article/details/5499810
        // https://www.cnblogs.com/gwwdq/p/9261615.html
        // tcp.h: https://blog.csdn.net/zhangyu627836411/article/details/80215746
        struct tcp_info info;
        int len = sizeof(info);
        getsockopt(fd, IPPROTO_TCP, TCP_INFO, &info, (socklen_t *)&len);
        if (info.tcpi_state != TCP_ESTABLISHED) {
            IF_LOG("server not connected");
            return false;
        }
    }
    // 初始化OpenSSL环境
    SSL_load_error_strings();
    SSLeay_add_ssl_algorithms();
    // 设置SSL协议版本为V2V3自适应
    meth_ = const_cast<SSL_METHOD*>(SSLv23_client_method());
    errBio_ = BIO_new_fd(STDOUT_FILENO, BIO_NOCLOSE);
    ctx_ = const_cast<SSL_CTX*>(SSL_CTX_new(meth_));
    CHK_NULL(ctx_);
    ssl_ = const_cast<SSL*>(SSL_new(ctx_));
    CHK_NULL(ssl_);
    SSL_set_fd(ssl_, fd);

    SSL_set_connect_state(ssl_);

#if 0
    SSL_set_verify_depth(ssl_, 0);
    SSL_set_verify(ssl_, SSL_VERIFY_NONE, nullptr);
    SSL_CTX_set_verify(ctx_, SSL_VERIFY_NONE, nullptr);
#endif
    // 启动SSL连接
    // int err = SSL_connect(ssl_); // -> handshake
    // CHK_SSL(err);

    return true;
}

void SslClient::SslInfo() {
    // 打印SSL连接的算法
    IF_LOG("SSL connection using: " << SSL_get_cipher(ssl_));
    // 获得服务端证书
    X509 *server_cert = SSL_get_peer_certificate(ssl_);
    CHK_NULL(server_cert);
    printf("Server certificate :\n");
    // 获得服务器证书名称
    char* str = X509_NAME_oneline(X509_get_subject_name(server_cert), 0, 0);
    CHK_NULL(str);
    printf("subject : %s\n", str);
    OPENSSL_free(str);
    // 获得服务器证书颁发者名称
    str = X509_NAME_oneline(X509_get_issuer_name(server_cert), 0, 0);
    CHK_NULL(str);
    printf("issuer: %s\n", str);
    //释放X509结构体
    X509_free(server_cert);

    // 证书验证
    // SSL_CTX_set_cert_verify_callback / SSL_CTX_set_verify / SSL_set_verify
//    long rc = SSL_get_verify_result(ssl_);
//    if(rc != X509_V_OK) {
//        if (rc == X509_V_ERR_DEPTH_ZERO_SELF_SIGNED_CERT || rc == X509_V_ERR_SELF_SIGNED_CERT_IN_CHAIN) {
//            fprintf(stderr, "self signed certificaten\n");
//        }
//        else {
//            fprintf(stderr, "Certificate verification error: %ld\n", SSL_get_verify_result(ssl_));
//            SSL_CTX_free(ctx_);
//            // return;
//        }
//    }
}

SslClient::SslClient()
    : ctx_    (nullptr)
    , ssl_    (nullptr)
    , meth_   (nullptr)
    , errBio_ (nullptr)
{}
SslClient::~SslClient(){
    if (!(ssl_ || ctx_)) {
        return;
    }
    SSL_shutdown(ssl_); // 发送SSL关闭消息
    SSL_free(ssl_);
    SSL_CTX_free(ctx_);
    // fd 由外部传入，外部释放
}

int SslClient::Write(void *data, size_t len) {
    if (ConnectState() != SslBase::SslState::SslConnected) {
        return -1;
    }
    return SSL_write(ssl_, data, len);
}
int SslClient::Read(void *buf, size_t len) {
    if (ConnectState() != SslBase::SslState::SslConnected) {
        return -1;
    }
    return SSL_read(ssl_, buf, len);
}
bool SslClient::SslHandShake() {
    int r = SSL_do_handshake(ssl_);
    if (r == 1) {
        m_state = SslBase::SslState::SslConnected;
        want_read_write_ = 0;
        SslInfo();
        IF_LOGW("SSL_do_handshake res: Connected");
        return true;
    }
    int err = SSL_get_error(ssl_, r);
    if (err == SSL_ERROR_WANT_WRITE) {
        m_state = SslBase::SslState::SslWantWrite;
        want_read_write_ = err;
        IF_LOGW("SSL_do_handshake res: WantReadWrite rw23: " << err);
        return false;
    } else if (err == SSL_ERROR_WANT_READ) {
        m_state = SslBase::SslState::SslWantRead;
        want_read_write_ = err;
        IF_LOGW("SSL_do_handshake res: WantReadWrite rw23: " << err);
        return false;
    } else {
        IF_LOGW("SSL_do_handshake res: error " << err);
        ERR_print_errors(errBio_);
        CHK_SSL(err);
    }
    return false;
}

bool SslClient::WantRead() {
    return want_read_write_ == SSL_ERROR_WANT_READ;
}
bool SslClient::WantWrite() {
    return want_read_write_ == SSL_ERROR_WANT_WRITE;
}
