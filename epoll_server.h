#ifndef EPOLL_CLIENT_H
#define EPOLL_CLIENT_H
#include "physical_socket_server.h"
#include "util.h"
#include "websockets.h"
#include "common.hpp" //guard

class EpollServer : public WssDataCallback, public sigslot::has_slots<> {
public:
    Security security_;
    Security *security() {
        return &security_;
    }

    EpollServer(SslBase::Type type = SslBase::Unknow)
    {
        security_.Create(type);

        websockets_.reset(new WebSockets);
        websockets_->WssInit(this, security()->ssl());
    }
    ~EpollServer() {
        IF_LOGW("~EpollServer");
    }

    //
    // override WssDataCallback  这里相当于同步模型和异步模型桥接的地方
    //
    std::unique_ptr <WebSockets> websockets_;

    DataQueue m_outQueue;           //! TODO是否需要加锁保护？单线程应该没必要
    DataQueue m_inQueue;
    size_t m_inDataSize = 0;
    int64_t Read(void *data, int64_t size) override {
        int readSize = 0;
        IF_LOG("Read m_inDataSize: " << m_inDataSize);
        auto guard = Wss::qScopeGuard([&]{
            m_inDataSize -= readSize;
        });
        // 客户端的读，一直是打开的，写是只有需要的时候打开？
        // 这是从缓冲区读取，该有的话一定有，没有的话就等待？
        // 增加异步使能等待的接口？
        // 这里读多次也不影响，每次都使能读取也可以。缓冲区要有尽可能多的数据
        //!来的数据都做了缓存，读到之后解包，因为没读完会一直触发所以读事件一直使能就行
        if (!m_inQueue.size()) {
            return -1;
        }

        DataArray &front = m_inQueue.front();
        if (size < front.size()) {
            memcpy(data, front.data(), size);
            front.erase(front.begin(), front.begin()+size); // read more data
            readSize = size;
            IF_LOG("read more 1");
            return readSize;
        }
        if (size == front.size()) {
            memcpy(data, front.data(), size);
            m_inQueue.pop();
            IF_LOG("pop");
            readSize = size;
            return readSize;
        }
        // 读取到的数据有可能是不够的
        while (m_inQueue.size() && size) {
            IF_LOG("Read multi buffer");
            int cpySize = front.size() > size ? size : front.size();
            memcpy(data, front.data(), cpySize);
            if (cpySize >= size) {
                m_inQueue.pop();
            } else {
                front.erase(front.begin(), front.begin()+cpySize);
            }
            size -= cpySize;
            readSize += cpySize;
            front = m_inQueue.front();
        }
        return readSize;
    }
    int64_t Write(void *data, int64_t size) override {
        IF_LOG("write buffer " << size);
        DataArray temp(size);
        memcpy(temp.data(), data, size);
        m_outQueue.push(temp);
        //! 返回值暂时不需要管
        return size;
    }
    bool maskEnabled() override { return false; }
    bool isReadyRead() override { return m_inQueue.size(); }
    int64_t bytesAvailable() override {
        IF_LOG("bytesAvailable: " << m_inDataSize << " " << m_inQueue.size());
        return m_inDataSize;
    }
    void OnTextMessage(const std::string &text) override {
        IF_LOG("OnTextMessage: " << std::endl << text << std::endl);
        static int count = 0;
        if (count < 10) {
            count++;
            sendTextMessage("i am server " + std::to_string(count));
            m_asyncSocket->WantReadWrite(true, true);
        }
    }
    void OnFailedMessage(const std::string &reason) override {
        IF_LOG("closed reason: " << reason);
    }

    int sendTextMessage(const std::string &msg) {
        return websockets_->doWriteFrames(msg.data(), msg.length(), false);
    }
    //! 握手前要确保已经 ssl()->Init！
    void HandShakeUpdate(AsyncSocket *socket) {
        if (security_.ssl()->ConnectState() == SslBase::SslConnected) {
            IF_LOGW("SslHandShake Connected");
            socket->WantReadWrite(true, false); //! 和客户端不同，Wss握手初始化
            return;
        }
        if (security_.ssl()->ConnectState() == SslBase::SslNotConnected) {
            IF_LOGW("SslHandShake not Connected");
            socket->WantReadWrite(true, false); //! 和客户端不同，Ssl握手初始化
            return;
        }
        if (security_.ssl()->ConnectState() == SslBase::SslWantRead) {
            socket->WantReadWrite(true, false);
            return;
        }
        if (security_.ssl()->ConnectState() == SslBase::SslWantWrite) {
            socket->WantReadWrite(false, true);
            return;
        }
        return;
    }
    inline void OnServerAcceptRead(AsyncSocket* socket) {
        if (((SocketDispatcher *)socket)->IsDescriptorClosed()) {
            socket->DeleteLater();
            return;
        }
        if (security()->ssl()->ConnectState() != SslBase::SslConnected) {
            security()->ssl()->SslHandShake();
            HandShakeUpdate(socket);
            return;
        }
        if (websockets_->HandShakeState() != WebSockets::WssFinished) {
            WebSockets::WssState state = websockets_->WssHandShake(true);
            if (state == WebSockets::WantWrite) {
                socket->WantReadWrite(false, true);
            }
            if (state == WebSockets::WssFinished) {
                socket->WantReadWrite(true, true);
            }
            return;
        }

        //!TODO: 完善时间戳的获取
        int64_t timestamp = 0;
        char buffer[512] = {0};
        // 不能连续读两次，所以只能buffer足够大，多次读取
        // 如果一次没读完就触发了可写信号呢？根据payload判断，根据协议自身的约定判断
        int nRead = socket->Recv(buffer, sizeof(buffer), &timestamp);
        if (nRead <= 0) {
            IF_LOG("nRead < 0");
            exit(1);
        }
        if (nRead > 0) {
            DataArray temp(nRead);
            mempcpy(temp.data(), buffer, nRead);
            m_inQueue.push(temp);
            m_inDataSize += nRead;
        }
        bool toWrite = m_outQueue.size();
        socket->WantReadWrite(true, socket->WriteEvent());
        bool res = websockets_->process(); // 解析读取到的数据
    }
    // 不能在槽函数里面 epoll_wait，导致不好处理异常断开的socket
    inline void OnServerAcceptWrite(AsyncSocket* socket) {
        if (((SocketDispatcher *)socket)->IsDescriptorClosed()) {
            socket->DeleteLater();
            return;
        }
        if (security()->ssl()->ConnectState() != SslBase::SslConnected) {
            security()->ssl()->SslHandShake();
            HandShakeUpdate(socket);
            return;
        }
        if (websockets_->HandShakeState() != WebSockets::WssFinished) {
            WebSockets::WssState state = websockets_->WssHandShake(true);
            if (state == WebSockets::WantRead) {
                socket->WantReadWrite(true, false);
            }
            if (state == WebSockets::WssFinished) {
                socket->WantReadWrite(true, true);
            }
            return;
        }

        DataArray &front = m_outQueue.front();
        if (m_outQueue.size()) {
            int nWrite = socket->Send(front.data(), front.size());
            if (nWrite <= 0) {
                IF_LOG("nWrite < 0");
                exit(1);
            }
            if (nWrite == front.size()) {
                IF_LOG("m_outQueue write pop");
                m_outQueue.pop();
            } else if (nWrite > 0) {
                IF_LOG("m_outQueue write erase");
                // 把写进去的部分擦除了下次继续进来写
                front.erase(front.begin(), front.begin() + nWrite);
            } else {
                IF_LOG("m_outQueue write error");
                //!TODO:完全写错误暂时不处理，尝试继续进入写事件，
                //! 然后epoll_wait结束会判断socket是不是关闭了
            }
        }
        //! 输出队列为空关闭写事件
        bool toWrite = m_outQueue.size();
        socket->WantReadWrite(true, toWrite);
    }
    inline void OnServerClose(AsyncSocket* socket, int err) {
        IF_LOG("OnServerClose.................1");
        socket->DeleteLater();
        //! 同时删除 accept 后的 EpollServer
        delete this;
    }
};
#endif //EPOLL_CLIENT_H
