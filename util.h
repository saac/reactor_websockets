#ifndef UTIL_H
#define UTIL_H
#include "ep_common.h"
#include <signal.h>
#include <functional>
#include <map>
#include <iomanip>

namespace EpUtil {

// linux定时器的实现：https://blog.csdn.net/renlonggg/article/details/90715550
class Timer {
    Timer() {
        if(signal(SIGALRM, SigFun) == SIG_ERR){
            perror("signal\n");
        }
    }
    ~Timer() {}
public:
    static Timer *instance() {
        static Timer *timer = new Timer;
        return timer;
    }
    void set(int time, std::function<void()>func, bool repeat = false) {
        m_cur_func = func;

        struct itimerval tv;
        tv.it_value.tv_usec = 0;
        tv.it_value.tv_sec = time;
        tv.it_interval.tv_usec = 0;
        tv.it_interval.tv_sec = repeat ? time : 0;

        if (setitimer(ITIMER_REAL, &tv, nullptr) != 0) {
            perror("setitimer");
        }
    }

private:
    static void SigFun(int signo){
        // std::function<void()> func = instance()->m_timeRecord.begin()->second;
        // instance()->m_timeRecord.erase(instance()->m_timeRecord.begin());
        // func();
        instance()->m_cur_func();
    }
    std::function<void()> m_cur_func;
    std::map<int, std::function<void()>, std::less_equal<int>> m_timeRecord;
};

// http://t.zoukankan.com/hustcpp-p-12155754.html
// http://www.bytekits.com/cppstdlib/cppstdlib-put-time.html
inline std::string gmtTimeNow(const char *fmt) {
    auto now = std::chrono::system_clock::now();
    auto itt = std::chrono::system_clock::to_time_t(now);

    std::ostringstream ss;
    ss << std::put_time(gmtime(&itt), fmt);
    return ss.str();
}

template <typename T>
inline void qSwap(T &value1, T &value2)
{
    using std::swap;
    swap(value1, value2);
}

}
#endif//UTIL_H
