#include <QApplication>
#include <QtCore/QObject>
#include <QtWebSockets/QWebSocket>
#include "echo_client.h"

int main(int argc, char *argv[]) {
    QApplication app(argc, argv);
    EchoClient client(QUrl(QStringLiteral("wss://192.168.31.184:8888")), false);
    client.handleMessage([&](QString msg){
        qInfo() << "Recv: " << msg;
        static int i  = 0;
        if (i < 2) {
            // client.sendTextMessage(QString("send from client: %1").arg(i));
            client.sendTextMessage("i am client: 2");
            i++;
        }
    });
    QObject::connect(&client, &EchoClient::connectSuccess, [&]{
        // client.sendTextMessage("xxxxxxxxx");
        client.sendTextMessage("i am client: 1");
    });
    return app.exec();
}

