#include "echo_server.h"
#include "QtWebSockets/qwebsocketserver.h"
#include "QtWebSockets/qwebsocket.h"
#include <QtCore/QDebug>
#include <QCoreApplication>
#include <QFile>
#include <QSslKey>

QT_USE_NAMESPACE

// https://blog.csdn.net/cqchengdan/article/details/97619483
EchoServer::EchoServer(quint16 port, bool debug, QObject *parent)
    : QObject(parent)
    , m_pWebSocketServer(new QWebSocketServer(QStringLiteral("Wss Server"),
                                            /*QWebSocketServer::NonSecureMode*/
                                              QWebSocketServer::SecureMode, this))
    , m_debug(debug)
{
    QFile certFile(CERT_PEM);
    QFile keyFile(KEY_PEM);
    if(!certFile.open(QIODevice::ReadOnly)){
        qDebug() << "certfile open failed...";
    }
    if(!keyFile.open(QIODevice::ReadOnly)){
        qDebug() << "keyfile open failed...";
    }
    QSslCertificate cert(&certFile, QSsl::Pem);
    QSslKey key(&keyFile, QSsl::Rsa, QSsl::Pem);
    certFile.close();
    keyFile.close();

    QSslConfiguration config = m_pWebSocketServer->sslConfiguration();
    config.setPeerVerifyMode(QSslSocket::VerifyNone);
    config.setLocalCertificate(cert);
    config.setPrivateKey(key);
    config.setProtocol(QSsl::TlsV1SslV3);
    m_pWebSocketServer->setSslConfiguration(config);

    // 用于接受客户端连接
    if (m_pWebSocketServer->listen(QHostAddress::Any, port)) {
        if (m_debug)
            qDebug() << "Echoserver listening on port" << port;
        connect(m_pWebSocketServer, &QWebSocketServer::newConnection,
                this, &EchoServer::onNewConnection);
        // connect(m_pWebSocketServer1, &QWebSocketServer::closed, qApp, &QCoreApplication::quit);
        connect(m_pWebSocketServer, &QWebSocketServer::sslErrors, [this](const QList<QSslError> &errors){
            qInfo() << "Ssl Error: " << errors;
        });
    }
}

EchoServer::~EchoServer()
{
    m_pWebSocketServer->close();
}

void EchoServer::onNewConnection()
{
    QWebSocket *pSocket = m_pWebSocketServer->nextPendingConnection();

    connect(pSocket, &QWebSocket::textMessageReceived, this, &EchoServer::processTextMessage);
    // connect(pSocket, &QWebSocket::binaryMessageReceived, this, &EchoServer::processBinaryMessage);
    connect(pSocket, &QWebSocket::disconnected, this, &EchoServer::socketDisconnected);
    m_clients.push_back(pSocket);
}

void EchoServer::processTextMessage(QString message)
{
    qInfo() << "Recv: " << message;
    QWebSocket *socket = qobject_cast<QWebSocket *>(sender());
    static int ch = 0;
    socket->sendTextMessage(QString("send from server ").append(std::to_string(ch++).c_str()));
}

void EchoServer::socketDisconnected()
{
    QWebSocket *pClient = qobject_cast<QWebSocket *>(sender());
    if (m_debug)
        qDebug() << "socketDisconnected:" << pClient;
    if (pClient) {
        m_clients.removeAll(pClient);
        pClient->deleteLater();
    }
}
