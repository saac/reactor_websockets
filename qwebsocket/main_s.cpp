#include <QApplication>
#include <QtCore/QObject>
#include <QtWebSockets/QWebSocket>
#include "echo_server.h"

int main(int argc, char *argv[]) {
    QApplication app(argc, argv);
    EchoServer server(8888);
    return app.exec();
}
