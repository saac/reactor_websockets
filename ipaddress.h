#ifndef IPADDRESS_H
#define IPADDRESS_H
#include <sys/socket.h>
#include <stdio.h>
#include <string.h>
#include <netinet/in.h>
#include "common.hpp"
#include <iostream>

class IPAddress;
// Prefixes used for categorizing IPv6 addresses.
static const in6_addr kV4MappedPrefix = {{{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0xFF, 0xFF, 0}}};
static const in6_addr kIPv6PublicAddr = {
    {{0x24, 0x01, 0xfa, 0x00, 0x00, 0x04, 0x10, 0x00, 0xbe, 0x30, 0x5b, 0xff,
      0xfe, 0xe5, 0x00, 0xc3}}};

bool IPIsHelper(const IPAddress& ip, const in6_addr& tomatch, int length);
bool IPIsV4Mapped(const IPAddress& ip);
in_addr ExtractMappedAddress(const in6_addr& in6);
inline uint32_t NetworkToHost32(uint32_t n) {
    return be32toh(n);
}
bool IPIsUnspec(const IPAddress& ip);

// Version-agnostic IP address class, wraps a union of in_addr and in6_addr.
class IPAddress {
public:
    IPAddress()
        : family_ (AF_UNSPEC)
    {
        ::memset(&u_, 0, sizeof(u_));
    }

    explicit IPAddress(const in_addr& ip4)
        : family_ (AF_INET)
    {
        memset(&u_, 0, sizeof(u_));
        u_.ip4 = ip4;
    }

    explicit IPAddress(const in6_addr& ip6)
        : family_(AF_INET6)
    {
        u_.ip6 = ip6;
    }

    explicit IPAddress(uint32_t ip_in_host_byte_order)
        : family_(AF_INET)
    {
        memset(&u_, 0, sizeof(u_));
        u_.ip4.s_addr = Wss::HostToNetwork32(ip_in_host_byte_order);
    }

    IPAddress(const IPAddress& other)
        : family_(other.family_)
    {
        ::memcpy(&u_, &other.u_, sizeof(u_));
    }

    virtual ~IPAddress() {}

    const IPAddress& operator=(const IPAddress& other)
    {
        family_ = other.family_;
        ::memcpy(&u_, &other.u_, sizeof(u_));
        return *this;
    }

    bool operator==(const IPAddress& other) const;
    bool operator!=(const IPAddress& other) const;
    bool operator<(const IPAddress& other) const;
    bool operator>(const IPAddress& other) const;

#if 0
    inline std::ostream& operator<<(  // no-presubmit-check TODO(webrtc:8982)
                                      std::ostream& os) {           // no-presubmit-check TODO(webrtc:8982)
        return os << ToString();
    }
#endif  // WEBRTC_UNIT_TEST

    int family() const {
        return family_;
    }
    inline in_addr ipv4_address() const{
        return u_.ip4;
    }
    inline in6_addr ipv6_address() const{
        return u_.ip6;
    }

    // Returns the number of bytes needed to store the raw address.
    size_t Size() const{
        switch (family_) {
        case AF_INET:
            return sizeof(in_addr);
        case AF_INET6:
            return sizeof(in6_addr);
        }
        return 0;
    }

    // Wraps inet_ntop.
    std::string ToString() const{
        if (family_ != AF_INET && family_ != AF_INET6) {
            return std::string();
        }
        char buf[INET6_ADDRSTRLEN] = {0};
        const void* src = &u_.ip4;
        if (family_ == AF_INET6) {
            src = &u_.ip6;
        }
        if (!inet_ntop(family_, src, buf, sizeof(buf))) {
            return std::string();
        }
        return std::string(buf);
    }

    // Same as ToString but anonymizes it by hiding the last part.
    std::string ToSensitiveString() const{
        switch (family_) {
        case AF_INET: {
            std::string address = ToString();
            size_t find_pos = address.rfind('.');
            if (find_pos == std::string::npos)
                return std::string();
            address.resize(find_pos);
            address += ".x";
            return address;
        }
        case AF_INET6: {
            std::string result;
            result.resize(INET6_ADDRSTRLEN);
            in6_addr addr = ipv6_address();
            size_t len = snprintf(&(result[0]), result.size(), "%x:%x:%x:x:x:x:x:x",
                    (addr.s6_addr[0] << 8) + addr.s6_addr[1],
                    (addr.s6_addr[2] << 8) + addr.s6_addr[3],
                    (addr.s6_addr[4] << 8) + addr.s6_addr[5]);
            result.resize(len);
            return result;
        }
        }
        return std::string();
    }

    // Returns an unmapped address from a possibly-mapped address.
    // Returns the same address if this isn't a mapped address.
    IPAddress Normalized() const {
        if (family_ != AF_INET6) {
            return *this;
        }
        if (!IPIsV4Mapped(*this)) {
            return *this;
        }
        in_addr addr = ExtractMappedAddress(u_.ip6);
        return IPAddress(addr);
    }

    // Returns this address as an IPv6 address.
    // Maps v4 addresses (as ::ffff:a.b.c.d), returns v6 addresses unchanged.
    IPAddress AsIPv6Address() const{
        if (family_ != AF_INET) {
            return *this;
        }
        in6_addr v6addr = kV4MappedPrefix;
        ::memcpy(&v6addr.s6_addr[12], &u_.ip4.s_addr, sizeof(u_.ip4.s_addr));
        return IPAddress(v6addr);
    }

    // For socketaddress' benefit. Returns the IP in host byte order.
    uint32_t v4AddressAsHostOrderInteger() const{
        if (family_ == AF_INET) {
            return NetworkToHost32(u_.ip4.s_addr);
        } else {
            return 0;
        }
    }

    // Get the network layer overhead per packet based on the IP address family.
    int overhead() const{
        switch (family_) {
        case AF_INET:  // IPv4
            return 20;
        case AF_INET6:  // IPv6
            return 40;
        default:
            return 0;
        }
    }

    // Whether this is an unspecified IP address.
    bool IsNil() const{
        return IPIsUnspec(*this);
    }

private:
    int family_;
    union {
        in_addr ip4;
        in6_addr ip6;
    } u_;
};

#endif // IPADDRESS_H
