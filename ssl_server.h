#ifndef SSL_SERVER_H
#define SSL_SERVER_H

#include <iostream>
#include <string>

#include <openssl/evp.h>
#include <openssl/pem.h>
#include <openssl/ssl.h>
#include <openssl/err.h>
#include <openssl/x509.h>

#include "async_socket.hpp"

#include "ssl_base.h"

#define CERTF "/home/alex/桌面/node_server/cert.pem" // 服务端证书
#define KEYF  "/home/alex/桌面/node_server/key.pem"  // 服务器端私钥
// #define ROOTCERTF "root.cer"    // 根证书

class SslServer : public SslBase {
public:
    bool ContextInit();

    // 需要对每个accept的fd进行初始化
    bool SslInit(int accept_fd);

    void SslInfo();

    // 合并 cs 端的 handshark、read、write
    bool SslHandShake();

    SslState ConnectState() {
        return m_state;
    }

    SslServer(const std::string &cert_path = CERTF,
              const std::string &key_path = KEYF);
    ~SslServer();

    bool Init(int fd) override {
        if (m_sslType != SslBase::Unknow) {
            // IF_LOGFE("xxxxxxxxxxxxxxxxxxxxxx");
            return true;
        }
        m_sslType = SslBase::Server;
        return SslInit(fd);
    }
    int Write(void *data, size_t len);
    int Read(void *buf, size_t len);
    bool WantRead() {
        return want_read_write_ == SSL_ERROR_WANT_READ;
    }
    bool WantWrite() {
        return want_read_write_ == SSL_ERROR_WANT_WRITE;
    }

private:
    SSL_CTX     *ctx_;              // SSL上下文句柄
    SSL         *ssl_;              // SSL结构体指针
    // X509     *server_cert_;      // X509 结构体，用户保存服务器端证书
    SSL_METHOD  *meth_;             // SSL 协议
    std::string cert_path_;
    std::string key_path_;

    SslState    m_state;
    int want_read_write_;
};

#endif//SSL_SERVER_H
